#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"

#include "assembler/ast.h"
#include "assembler/assembler.h"

int main(int argc, char ** argv) {

  /**
   * ; Program under consideration:
   * bra main
   * subr:
   *  ldc 1
   *  ldc 2
   *  add
   *  ret
   * main:
   *  bsr subr
   *  halt
   */
  ProgramT * p = NULL;
  NodeT * i1 = createInstruction(ASM_BRA, LabelArgument, "main")
      , * i2 = createLabel("subr")
      , * i3 = createInstruction(ASM_LDC, ValueArgument, 0x01)
      , * i4 = createInstruction(ASM_LDC, ValueArgument, 0x02)
      , * i5 = createInstruction(ASM_ADD)
      , * i6 = createInstruction(ASM_RET)
      , * i7 = createLabel("main")
      , * i8 = createInstruction(ASM_BSR, LabelArgument, "subr")
      , * i9 = createInstruction(ASM_HALT)
      ;

  // Program creation (append)
  p = appendProgram(
        appendProgram(
          appendProgram(
            appendProgram(
              appendProgram(
                appendProgram(
                  appendProgram(
                    appendProgram(
                      appendProgram(p, i1), i2), i3), i4), i5), i6), i7), i8), i9);

  prettyPrintProgram(p);

  return EXIT_SUCCESS;
}
