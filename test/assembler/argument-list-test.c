#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"

#include "assembler/ast.h"

// wrapper to handle the va_list
ArgumentListT * wrapper(int argc, ...) {
  va_list argv;
  va_start(argv, argc);
  ArgumentListT * t = newArgumentListN(argv, argc);
  va_end(argv);
  return t;
}

int main(int argc, char ** argv) {

  // Test the constructor
  ArgumentListT * a1 = newArgumentList(ValueArgument, 0xFF)
              , * a2 = newArgumentList(AddressArgument, 0x7ab)
              , * a3 = newArgumentList(RegisterArgument, 0)
              , * a4 = newArgumentList(LabelArgument, (void *) "function_mine")
              ;

  // Test the pretty printer, one at a time
  prettyPrintArgumentList(a1);
  prettyPrintArgumentList(a2);
  prettyPrintArgumentList(a3);
  prettyPrintArgumentList(a4);

  // Test append
  ArgumentListT * as = appendArgument(appendArgument(appendArgument(a1, a2), a3), a4);

  // test Pretty printer, on list
  prettyPrintArgumentList(as);

  // test replace.
  replaceArgument(a2, LabelArgument, "replaced");
  prettyPrintArgumentList(as);

  // test free, then print the results.
  freeArgumentList(as);

  // Hasn't segfaulted yet... probably because the structs lived on our own
  // heap and so it is no segviolation to dereference these addresses...
  prettyPrintArgumentList(as);

  printf("Length of as is %u\n", lengthArgumentList(as));

  // test newArgumentListN
  ArgumentListT * ns = wrapper(8, ValueArgument,   0xFF
                                , AddressArgument, 0xAA
                                , RegisterArgument, SSM_PC
                                , LabelArgument,   "myLabel123"
                                );
  prettyPrintArgumentList(ns);

  printf("Length of ns is %u\n", lengthArgumentList(ns));

  freeArgumentList(ns);

  return EXIT_SUCCESS;
}
