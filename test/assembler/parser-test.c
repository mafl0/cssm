#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/stat.h>
#include <limits.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"

#include "assembler/ast.h"
#include "assembler/parser.h"
#include "assembler/lexer.h"

extern ProgramT * getAST(const char * prog);

int main(int argc, char ** argv) {

  char * infile = NULL
     ,   option = '\0'
     ;

  struct stat buf;

  FILE * h = NULL;

  char * fbuf = NULL;

  while ((option = getopt(argc, argv, "f:d")) != -1) {
    switch (option) {
      case 'f':
        infile = strndup(optarg, PATH_MAX);
        break;
      case 'd':
        yydebug = 1;
        break;
      default:
        break;
    }
  }

  if (stat(infile, &buf) == -1) {
    perror(NULL);
  } else {
    fbuf = (char *) malloc(sizeof(char) * buf.st_size);
    h = fopen(infile, "rb");
    fread(fbuf, sizeof(char), buf.st_size, h);
    fclose(h);
  }

  ProgramT * e = getAST(fbuf);
  prettyPrintProgram(e);
  freeProgram(e);

  return EXIT_SUCCESS;
}
