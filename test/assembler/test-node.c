#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"
#include "assembler/ast.h"

int main(int argc, char ** argv) {

  // Create some instructions
  NodeT * i1, *i2, *i3, *i4;

  i1 = createInstruction(ASM_ADD);   
  i2 = createInstruction(ASM_LDC, ValueArgument, 0x01);   
  i3 = createInstruction(ASM_SWPRR, RegisterArgument, SSM_PC, RegisterArgument, SSM_SP);   
  i4 = createInstruction(ASM_ANNOTE, ValueArgument, 0, ValueArgument, 0, ValueArgument, 0, ValueArgument, 0, ValueArgument, 0);

  // Create a label
  NodeT * l1;
  l1 = createLabel("myFunction123");

  // pretty printing
  prettyPrintNode(i1);
  prettyPrintNode(i2);
  prettyPrintNode(i3);
  prettyPrintNode(i4);
  prettyPrintNode(l1);


  // free
  freeNode(i1);
  freeNode(i2);
  freeNode(i3);
  freeNode(i4);
  freeNode(l1);


  return EXIT_SUCCESS;
}
