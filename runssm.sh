#!/usr/bin/env bash

# Shell script for running .ssm assembly files directly. This script will
# assemble the source code into a ssm machine code binary stored in a temporary 
# file. The code size is automatically computed from the file size. By default
# the SSM runs with 1k SSM words of stack memory and 1k SSM words of heap memory

SSMDIR="build"

SSMASM="$SSMDIR/ssmasm"
SSM="$SSMDIR/ssm"

STACK_SIZE=1024
HEAP_SIZE=1024
DEBUG=0

function main() {

  while getopts "f:dS:H:" OPTION; do

  case "$OPTION" in
    f) INFILE="$OPTARG"     ;;
    d) DEBUG=1              ;;
    S) STACK_SIZE="$OPTARG" ;;
    H) HEAP_SIZE="$OPTARG"  ;;
    *) ;;
  esac
  done

  OUTFILE=$(mktemp)

  "$SSMASM" -f "$INFILE" -o "$OUTFILE"

  OUTFILE_BYTES=$(stat --format="%s" "$OUTFILE")
  CODE_SIZE=$(echo "1 + $OUTFILE_BYTES / 4" | bc)

  echo "Running $INFILE"
  echo "SSM word size: 32 bits"
  echo "Code size: $CODE_SIZE ssm words (computed)"
  echo "Stack size: $STACK_SIZE ssm words (preset)"
  echo "Heap size: $HEAP_SIZE ssm sords (preset)"

  if [[ "$DEBUG" == 1 ]]; then
    SSM="$SSMDIR/ssmdbg"
  fi

  "$SSM" -C "$CODE_SIZE" -S "$STACK_SIZE" -H "$HEAP_SIZE" -f "$OUTFILE"

  rm "$OUTFILE"
}

main $@
