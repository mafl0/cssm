/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_SRC_ASSEMBLER_PARSER_H_INCLUDED
# define YY_YY_SRC_ASSEMBLER_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    T_COLON = 258,                 /* T_COLON  */
    T_ADD = 259,                   /* T_ADD  */
    T_AJS = 260,                   /* T_AJS  */
    T_AND = 261,                   /* T_AND  */
    T_ANNOTE = 262,                /* T_ANNOTE  */
    T_BRA = 263,                   /* T_BRA  */
    T_BRF = 264,                   /* T_BRF  */
    T_BRT = 265,                   /* T_BRT  */
    T_BSR = 266,                   /* T_BSR  */
    T_DIV = 267,                   /* T_DIV  */
    T_EQ = 268,                    /* T_EQ  */
    T_GE = 269,                    /* T_GE  */
    T_GT = 270,                    /* T_GT  */
    T_HALT = 271,                  /* T_HALT  */
    T_JSR = 272,                   /* T_JSR  */
    T_LDA = 273,                   /* T_LDA  */
    T_LDAA = 274,                  /* T_LDAA  */
    T_LDC = 275,                   /* T_LDC  */
    T_LDH = 276,                   /* T_LDH  */
    T_LDL = 277,                   /* T_LDL  */
    T_LDLA = 278,                  /* T_LDLA  */
    T_LDMA = 279,                  /* T_LDMA  */
    T_LDMH = 280,                  /* T_LDMH  */
    T_LDML = 281,                  /* T_LDML  */
    T_LDMS = 282,                  /* T_LDMS  */
    T_LDR = 283,                   /* T_LDR  */
    T_LDRR = 284,                  /* T_LDRR  */
    T_LDS = 285,                   /* T_LDS  */
    T_LDSA = 286,                  /* T_LDSA  */
    T_LE = 287,                    /* T_LE  */
    T_LINK = 288,                  /* T_LINK  */
    T_LT = 289,                    /* T_LT  */
    T_MOD = 290,                   /* T_MOD  */
    T_MUL = 291,                   /* T_MUL  */
    T_NE = 292,                    /* T_NE  */
    T_NEG = 293,                   /* T_NEG  */
    T_NOP = 294,                   /* T_NOP  */
    T_NOT = 295,                   /* T_NOT  */
    T_OR = 296,                    /* T_OR  */
    T_RET = 297,                   /* T_RET  */
    T_STA = 298,                   /* T_STA  */
    T_STH = 299,                   /* T_STH  */
    T_STL = 300,                   /* T_STL  */
    T_STMA = 301,                  /* T_STMA  */
    T_STMH = 302,                  /* T_STMH  */
    T_STML = 303,                  /* T_STML  */
    T_STMS = 304,                  /* T_STMS  */
    T_STR = 305,                   /* T_STR  */
    T_STS = 306,                   /* T_STS  */
    T_SUB = 307,                   /* T_SUB  */
    T_SWP = 308,                   /* T_SWP  */
    T_SWPR = 309,                  /* T_SWPR  */
    T_SWPRR = 310,                 /* T_SWPRR  */
    T_TRAP = 311,                  /* T_TRAP  */
    T_UNLINK = 312,                /* T_UNLINK  */
    T_XOR = 313,                   /* T_XOR  */
    T_PC = 314,                    /* T_PC  */
    T_HP = 315,                    /* T_HP  */
    T_MP = 316,                    /* T_MP  */
    T_R0 = 317,                    /* T_R0  */
    T_R1 = 318,                    /* T_R1  */
    T_R2 = 319,                    /* T_R2  */
    T_R3 = 320,                    /* T_R3  */
    T_R4 = 321,                    /* T_R4  */
    T_R5 = 322,                    /* T_R5  */
    T_R6 = 323,                    /* T_R6  */
    T_R7 = 324,                    /* T_R7  */
    T_RR = 325,                    /* T_RR  */
    T_SP = 326,                    /* T_SP  */
    T_SR = 327,                    /* T_SR  */
    T_CONSTANT = 328,              /* T_CONSTANT  */
    T_LABEL = 329                  /* T_LABEL  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 48 "src/assembler/parser.y"

  // Argument types
  ssm_memory_t   ssmVal;
  ssm_memory_ptr ssmPtr;
  char *         ssmLabel;
  ssm_register_t ssmReg;

  // Instructions and labels
  NodeT * node;

  // Program
  ProgramT * program;

#line 152 "src/assembler/parser.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif




int yyparse (ProgramT ** program, void * scanner);


#endif /* !YY_YY_SRC_ASSEMBLER_PARSER_H_INCLUDED  */
