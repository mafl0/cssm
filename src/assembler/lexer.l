%{
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"
#include "ast.h"
#include "parser.h"

%}

%option bison-bridge case-insensitive never-interactive nodefault nounistd noyywrap reentrant warn yylineno

WHITESPACE      [ \n\r\t]+
TABS_AND_SPACES [ \t]*
NEWLINE         (\r|\n|\r\n)

LABEL           [a-zA-Z_][a-zA-Z0-9_]*

DNUM  -?[0-9]+
HNUM  -?"0x"[0-9a-fA-F]+
ONUM  -?"0o"[0-7]+
BNUM  -?"0b"[01]+

/**
 * States the lexer can be in. Depending on the state, the lexer accepts
 * different tokens 
 */
%x args

/**
 * Example:
 *
 * ldc 2
 * ^
 * |
 * +- state INITIAL, token T_LDC is accepted and we enter state `args'
 *
 * ldc 2
 *     ^
 *     |
 *      +- state `args ', token T_CONSTANT is accepted and we remain in state 
 *         `args'
 * ldc $2
 *       ^
 *       |
 *       +- state `args', NEWLINE is matched and we reset the state 
 *          to INITIAL
 *
 * The reason we do this is because it is easier to get rid of comments, 
 * trailing and leading whitespace.
 *
 */

%%

<*>{TABS_AND_SPACES}+  
<*>{NEWLINE}             { BEGIN(INITIAL); }
<*>";"[^\r\n]* 

<args>{DNUM}             { // Decimal digits of the form -?[0-9]+
                           BEGIN(args);
                           sscanf(yytext, "%d", &yylval->ssmVal); 
                           return T_CONSTANT; 
                         }
<args>{HNUM}             { // Hex numbers of the form -?0x[0-9a-fA-F]+, ssm extension
                           BEGIN(args);
                           sscanf(yytext, "%x", (unsigned int *) &yylval->ssmVal); 
                           return T_CONSTANT; 
                         }
<args>{ONUM}             { // Octal numbers of the form -?0o[0-7]+, ssm extension
                           BEGIN(args);
                           ssm_memory_t mul = 1;
                           size_t offs = 0;

                           if ( yytext[offs] == '-') {
                             mul = -1;
                             offs += 1;
                           }

                           if (yytext[offs] == '0' && (yytext[offs+1] == 'o' || yytext[offs+1] == 'O')) {
                             offs += 2;
                           }

                           sscanf(yytext + offs, "%o", (unsigned int *) &yylval->ssmVal); 
                           yylval->ssmVal *= mul;

                           return T_CONSTANT; 
                         }
<args>{BNUM}             { // Binary numbers of the form -?0b[01]+, ssm extension
                           BEGIN(args);
                           ssm_memory_t mul = 1;
                           size_t offs = 0;

                           // Detect the minus (-)
                           if ( yytext[offs] == '-') {
                             mul = -1;
                             offs += 1;
                           }

                           // Skip 0b or 0B
                           if (yytext[offs] == '0' && (yytext[offs+1] == 'b' || yytext[offs+1] == 'B')) {
                             offs += 2;
                           }
                         
                           // Find the address of the last [01]+ string character.
                           char * lastChar = yytext + offs;
                           while (*lastChar == '0' || *lastChar == '1') ++lastChar;
                           --lastChar; // one too much

                           /** 
                            * Initialize yylval to 0, scan from LSB to MSB until
                            * we have reached the beginning of the [01]+ string
                            * (yytext + offs), or we have exceeded the size that
                            * an SSM word can carry (sizeof(ssm_memory_t) * 8)
                            */
                           yylval->ssmVal = 0;
                           for ( char * ptr = lastChar;
                                 (lastChar - ptr < (sizeof(ssm_memory_t) * 8)) && (yytext + offs <= ptr);
                                 ptr--)  {
                             yylval->ssmVal |= (*ptr - '0') << (lastChar - ptr);
                           }


                           // If the text began with '-', we want the inverse of
                           // the parsed number.
                           yylval->ssmVal *= mul;

                           return T_CONSTANT; 
                         }

<args>"pc"               { return T_PC;                  }
<args>"hp"               { return T_HP;                  }
<args>"mp"               { return T_MP;                  }
<args>"r0"               { return T_R0;                  }
<args>"r1"               { return T_R1;                  }
<args>"r2"               { return T_R2;                  }
<args>"r3"               { return T_R3;                  }
<args>"r4"               { return T_R4;                  }
<args>"r5"               { return T_R5;                  }
<args>"r6"               { return T_R6;                  }
<args>"r7"               { return T_R7;                  }
<args>"rr"               { return T_RR;                  }
<args>"sp"               { return T_SP;                  }
<args>"sr"               { return T_SR;                  }

<INITIAL>"add"           {              return T_ADD;    }
<INITIAL>"ajs"           { BEGIN(args); return T_AJS;    }
<INITIAL>"and"           {              return T_AND;    }
<INITIAL>"annote"        {              return T_ANNOTE; }
<INITIAL>"bra"           { BEGIN(args); return T_BRA;    }
<INITIAL>"brf"           { BEGIN(args); return T_BRF;    }
<INITIAL>"brt"           { BEGIN(args); return T_BRT;    }
<INITIAL>"bsr"           { BEGIN(args); return T_BSR;    }
<INITIAL>"div"           {              return T_DIV;    }
<INITIAL>"eq"            {              return T_EQ;     }
<INITIAL>"ge"            {              return T_GE;     }
<INITIAL>"gt"            {              return T_GT;     }
<INITIAL>"halt"          {              return T_HALT;   }
<INITIAL>"jsr"           {              return T_JSR;    }
<INITIAL>"ldaa"          { BEGIN(args); return T_LDAA;   }
<INITIAL>"lda"           { BEGIN(args); return T_LDA;    }
<INITIAL>"ldc"           { BEGIN(args); return T_LDC;    }
<INITIAL>"ldh"           { BEGIN(args); return T_LDH;    }
<INITIAL>"ldla"          { BEGIN(args); return T_LDLA;   }
<INITIAL>"ldl"           { BEGIN(args); return T_LDL;    }
<INITIAL>"ldma"          { BEGIN(args); return T_LDMA;   }
<INITIAL>"ldmh"          { BEGIN(args); return T_LDMH;   }
<INITIAL>"ldml"          { BEGIN(args); return T_LDML;   }
<INITIAL>"ldms"          { BEGIN(args); return T_LDMS;   }
<INITIAL>"ldr"           { BEGIN(args); return T_LDR;    }
<INITIAL>"ldrr"          { BEGIN(args); return T_LDRR;   }
<INITIAL>"lds"           { BEGIN(args); return T_LDS;    }
<INITIAL>"ldsa"          { BEGIN(args); return T_LDSA;   }
<INITIAL>"le"            {              return T_LE;     }
<INITIAL>"link"          { BEGIN(args); return T_LINK;   }
<INITIAL>"lt"            {              return T_LT;     }
<INITIAL>"mod"           {              return T_MOD;    }
<INITIAL>"mul"           {              return T_MUL;    }
<INITIAL>"ne"            {              return T_NE;     }
<INITIAL>"neg"           {              return T_NEG;    }
<INITIAL>"nop"           {              return T_NOP;    }
<INITIAL>"not"           {              return T_NOT;    }
<INITIAL>"or"            {              return T_OR;     }
<INITIAL>"ret"           {              return T_RET;    }
<INITIAL>"sta"           { BEGIN(args); return T_STA;    }
<INITIAL>"sth"           { BEGIN(args); return T_STH;    }
<INITIAL>"stl"           { BEGIN(args); return T_STL;    }
<INITIAL>"stma"          { BEGIN(args); return T_STMA;   }
<INITIAL>"stmh"          { BEGIN(args); return T_STMH;   }
<INITIAL>"stml"          { BEGIN(args); return T_STML;   }
<INITIAL>"stms"          { BEGIN(args); return T_STMS;   }
<INITIAL>"str"           { BEGIN(args); return T_STR;    }
<INITIAL>"sts"           { BEGIN(args); return T_STS;    }
<INITIAL>"sub"           {              return T_SUB;    }
<INITIAL>"swprr"         { BEGIN(args); return T_SWPRR;  }
<INITIAL>"swpr"          { BEGIN(args); return T_SWPR;   }
<INITIAL>"swp"           {              return T_SWP;    }
<INITIAL>"trap"          { BEGIN(args); return T_TRAP;   }
<INITIAL>"unlink"        {              return T_UNLINK; }
<INITIAL>"xor"           {              return T_XOR;    }

<*>{LABEL}               { yylval->ssmLabel = strndup(yytext, LABEL_BUF_SZ); 
                           return T_LABEL;   
                         }
<*>":"                   { return T_COLON;  }
