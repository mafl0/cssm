#ifndef __SSM_ASSEMBLER_NODE_H__
#define __SSM_ASSEMBLER_NODE_H__

typedef enum NodeType {
    LabelNode
  , InstructionNode
} NodeTypeT;

typedef struct Node {
  NodeTypeT type;
  
  // Instruction
  opcode_t opcode;
  struct ArgumentList * args;

  // Label
  char * label;

  // Bookkeeping for the assembler
  ssm_memory_ptr offset;       // Offset in the program
} NodeT ;

// Constructor for instructions. Create a new instruction node, with a variable
// argument list of format arg1Type, arg1Val, arg2Type, arg2Val ...
NodeT * createInstruction(opcode_t opcode, ...);

// Constructor for labels
NodeT * createLabel(char * label);

// Destructor. Free a node and its arguments.
void freeNode(NodeT * n);

// Pretty printer
void prettyPrintNode(NodeT * node);

#endif /* ifdef __SSM_ASSEMBLER_NODE_H__ */
