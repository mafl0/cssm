#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interpreter/model.h"
#include "interpreter/error.h"

#include "assembler/ast/argument.h"


// Turn off the pointer-to-int-cast warning for this function because it's actually fine
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpointer-to-int-cast"

// Insertion of data into a link depends on the type. We have to switch on the
// type and cast the value to the right type.
static void _insertArgumentData(ArgumentListT * a, ArgumentTypeT type, void * content) {
  switch (type) {
    case ValueArgument:
      a->val = (ssm_memory_t) content; 
      break;
    case RegisterArgument:
      a->reg = (ssm_register_t) content; 
      break;
    case AddressArgument:
      a->addr = (ssm_memory_ptr) content; 
      break;
    case LabelArgument:
      a->label = (char * ) strndup(content, LABEL_BUF_SZ);
      break;
    default:
      break;
  }
}

#pragma GCC diagnostic pop

ssm_memory_t getArgumentValue(ArgumentListT * arg) {
  ssm_memory_t ret;

  switch (arg->type) {
    case ValueArgument:
      ret = arg->val;
      break;
    case RegisterArgument:
      ret = arg->reg;
      break;
    case AddressArgument:
      ret = arg->addr;
      break;
    case LabelArgument:
      ret = 0; // TODO, cast to pointer? -> probably not...
      break;
    default:
      break;
  }

  return ret;
}

// Constructor
ArgumentListT * newArgumentList(ArgumentTypeT type, void * content) {
  ArgumentListT * as = malloc(sizeof(ArgumentListT));
  as->type = type;
  as->tail = NULL;
  _insertArgumentData(as, type, content);
  as->_last = as; // For single links, we are our own last
  return as;
}

// Make an argument list of size @n and populate it with the va_list values.
// Returns a pointer to the first;
ArgumentListT * newArgumentListN(va_list argv, size_t argc) {
  if (argc == 0) return NULL;

  ArgumentTypeT type = va_arg(argv, ArgumentTypeT);
  void * content     = va_arg(argv, void *);
  ArgumentListT * first = newArgumentList(type, content);
  ArgumentListT * ptr;

  for (size_t i = 1; i < argc; ++i) {
    type    = va_arg(argv, ArgumentTypeT);
    content = va_arg(argv, void *);
    ptr     = newArgumentList(type, content);
    appendArgument(first, ptr);
  }

  return first;
}

// Destructor
void freeArgumentList(ArgumentListT * as) {
  // Sentinel
  if (as == NULL) return;

  // Free the tail, then free the head.
  freeArgumentList(as->tail);
  free(as);
}

// Replace the link at the pointer with a new link of a different type
void replaceArgument(ArgumentListT * link , ArgumentTypeT type, void * content) {
  
  if (link == NULL) return;

  link->type = type;
  _insertArgumentData(link, type, content);
}

// Append a to as (create as:a)
ArgumentListT * appendArgument(ArgumentListT * as, ArgumentListT * a) {

  // Sentinels, return NULL if both are NULL, or the unaltered argument if one
  // of the two is NULL;
  if (a  == NULL && as == NULL) return NULL;
  if (a  == NULL) return as;
  if (as == NULL) return a ;

  as->_last->tail = a;
  as->_last = a;

  return as;
}

static void _prettyPrintArgument(ArgumentListT * a) {
  
  if (a == NULL) return;

  char * buf = NULL;

  switch (a->type) {
    case ValueArgument:
      buf = prettyPrintValue(a->val);
      printf("  * value:    %s\n", buf);
      break;
    case RegisterArgument:
      buf = prettyPrintRegister(a->reg);
      printf("  * register: %s\n", buf);
      break;
    case AddressArgument:
      buf = prettyPrintAddress(a->addr);
      printf("  * address:  %s\n", buf);
      break;
    case LabelArgument:
      printf("  * label:    \"%s\"\n", a->label);
      break;
    default:
      break;
  }

  free(buf);
}

void prettyPrintArgumentList(ArgumentListT * as) {
  if (as == NULL) return;

  printf("- arguments:\n");
  for (ArgumentListT * ptr = as; ptr != NULL; ptr = ptr->tail) {
    _prettyPrintArgument(ptr);
  }
}

size_t lengthArgumentList(ArgumentListT * as) {
  size_t i = 0;
  ArgumentListT * ptr = as;
  for(; ptr != NULL; ptr = ptr->tail)
    ++i;
  return i;
}
