#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"
#include "interpreter/error.h"

#include "assembler/ast/node.h"
#include "assembler/ast/program.h"

/**
 * Program constructor and helpers
 */
ProgramT * newProgramLink(NodeT * head) {

  ProgramT * p = NULL; 

  // Sentinel, we need to have a node to create a program
  if (head != NULL) {
    p = (ProgramT *) malloc(sizeof(ProgramT));
    p->head  = head;
    p->tail  = NULL;
    p->_last = p; // In a single link list, we are our own last
  }

  return p;
}

ProgramT * appendProgram(ProgramT * p, NodeT * node) {

  // Sentinels
  if (node == NULL && p == NULL) return NULL;
  if (node == NULL) return p;

  ProgramT * pNew = newProgramLink(node);

  // Sentinel, if p is NULL then the new link is the entire program
  if (p == NULL) return pNew;

  p->_last->tail = pNew;
  p->_last       = pNew; 

  return p;
}

ProgramT * consProgram(NodeT * node, ProgramT * p) {

  // Sentinels
  if (node == NULL && p == NULL) return NULL;
  if (node == NULL) return p;

  ProgramT * pNew = newProgramLink(node);

  // Sentinel, if p is NULL then the new link is the entire program
  if (p == NULL) return pNew;

  pNew->tail = p;
  pNew->_last = p->_last;

  return pNew;
}

// Destructor, free the program list from the heap.
void freeProgram(ProgramT * p) {
  if (p != NULL) {
    freeNode(p->head);
    freeProgram(p->tail);
    free(p);
  }
}
 
void prettyPrintProgram(ProgramT * p) {
  if (p != NULL) {
    for (ProgramT * ptr = p; ptr != NULL; ptr = ptr->tail)
      prettyPrintNode(ptr->head);
  }
}

