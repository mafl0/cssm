#ifndef __SSM_ASSEMBLER_AST_PROGRAM_H__
#define __SSM_ASSEMBLER_AST_PROGRAM_H__

/**
 * A program is a list of nodes. Nodes are mostly instructions but can also be
 * labels. An assembled program has no label nodes left.
 */
typedef struct Program {
  NodeT * head;
  struct Program * tail;

  // Make the /head/ always point to the last element of the list. For O(1)
  // append.
  struct Program * _last;
} ProgramT;

// Constructor, create a new program link.
ProgramT * newProgramLink(NodeT * head);

// Append a node to the program
ProgramT * appendProgram(ProgramT * p, NodeT * node);

// Cons (Prepend) a node to the program
ProgramT * consProgram(NodeT * node, ProgramT * p);

// Destructor
void freeProgram(ProgramT * p);

// Pretty Printer
void prettyPrintProgram(ProgramT * p);

#endif // __SSM_ASSEMBLER_AST_PROGRAM_H__
