#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"
#include "interpreter/error.h"

#include "assembler/ast/argument.h"
#include "assembler/ast/node.h"

// Allocate a new node, internal
static NodeT * _newNode() {
  NodeT * i = malloc(sizeof(NodeT));
  i->opcode = 0;
  i->args   = NULL;
  i->label  = NULL;
  return i;
}

// instructions
NodeT * createInstruction(opcode_t opcode, ...) {
  va_list argv; // = NULL?
  size_t argc = 0;

  switch(opcode) {
    case ASM_ADD:
    case ASM_AND:
    case ASM_DIV:
    case ASM_EQ:
    case ASM_GE:
    case ASM_GT:
    case ASM_HALT:
    case ASM_JSR:
    case ASM_LE:
    case ASM_LT:
    case ASM_MOD:
    case ASM_MUL:
    case ASM_NE:
    case ASM_NEG:
    case ASM_NOP:
    case ASM_NOT:
    case ASM_OR:
    case ASM_RET:
    case ASM_STH:
    case ASM_SUB:
    case ASM_SWP:
    case ASM_UNLINK:
    case ASM_XOR:
      argc = 0;
      break;
    case ASM_AJS:
    case ASM_BRA:
    case ASM_BRF:
    case ASM_BRT:
    case ASM_BSR:
    case ASM_LDA:
    case ASM_LDAA:
    case ASM_LDC:
    case ASM_LDH:
    case ASM_LDL:
    case ASM_LDLA:
    case ASM_LDR:
    case ASM_LDS:
    case ASM_LDSA:
    case ASM_LINK:
    case ASM_STA:
    case ASM_STL:
    case ASM_STMH:
    case ASM_STR:
    case ASM_STS:
    case ASM_SWPR:
    case ASM_TRAP:
      argc = 1;
      break;
    case ASM_LDMA:
    case ASM_LDMH:
    case ASM_LDML:
    case ASM_LDMS:
    case ASM_LDRR:
    case ASM_STMA:
    case ASM_STML:
    case ASM_STMS:
    case ASM_SWPRR:
      argc = 2;
      break;
    case ASM_ANNOTE:
      argc = 5;
      break;
    default:
      ssmError(SSMERR_INSTRUCTION_UNKNOWN, "Error: Unknown opcode encountered 0x%02zx\n", opcode);
      break;
  }

  // We handle argument assignment in the _newArgumentListN function. Here we
  // just need to know the amount of arguments to assign.
  va_start(argv, opcode); 
  NodeT * node = _newNode();
  node->opcode = opcode;
  node->type = InstructionNode;
  node->args = newArgumentListN(argv, argc);
  va_end(argv);

  return node;
}

// Label node
NodeT * createLabel(char * label) {
  if (label == NULL) label = "";
  NodeT * node = _newNode();
  node->type = LabelNode;
  node->label = strndup(label, LABEL_BUF_SZ);
  return node;
}

// Free a node, recursively freeing its arguments
void freeNode(NodeT * node) {
  if (node == NULL) return;

  freeArgumentList(node->args);
  free(node->label);
  free(node);
}

// Pretty printer
inline void prettyPrintNode(NodeT * node) {
  if (node == NULL) return;

  switch (node->type) {
    case InstructionNode:
      printf("Instruction Node:\n");
      printf("- opcode: 0x%02x\n", node->opcode);
      printf("- offset: 0x%04lx\n", node->offset);
      prettyPrintArgumentList(node->args);
      break;
    case LabelNode:
      printf("Label Node:\n");
      printf("- label:  \"%s\"\n", node->label);
      printf("- offset: 0x%04lx\n", node->offset);
      break;
    default:
      break;
  }
}

