#ifndef __SSM_ASSEMBLER_AST_ARGUMENT_H__
#define __SSM_ASSEMBLER_AST_ARGUMENT_H__

/**
 * Argument types that the SSM accepts as immediate arguments to instructions.
 * We either have values, registers, addresses or labels. Labels only exist in
 * an early stage of assembly, and are later resolved to /absolute/ addresses.
 */
typedef enum ArgumentType {
    ValueArgument
  , RegisterArgument
  , AddressArgument
  , LabelArgument
} ArgumentTypeT;

/**
 * Argument list shackle type. Each shackle has a type, data (depending on the
 * type), the length of the chain including itself, and a pointer to the next
 * shackle in the chain.
 */
typedef struct ArgumentList {
  ArgumentTypeT type;
  
  // Value arg
  ssm_memory_t   val;

  // Address arg
  ssm_memory_ptr addr;

  // Register arg
  ssm_register_t reg;

  // Label arg
  char *         label;

  // Tail
  struct ArgumentList * tail;

  struct ArgumentList * _last;
} ArgumentListT;

// Constructor, create a new list
ArgumentListT * newArgumentList(ArgumentTypeT type, void * content);

// Constructor, create a new list of a known length
ArgumentListT * newArgumentListN(va_list argv, size_t argc);

// Destructor, destroy a list bottom up to not lose pointers
void freeArgumentList(ArgumentListT * as);

// Replace the shackle at the pointer with a new shackle of a different type
void replaceArgument(ArgumentListT * shackle, ArgumentTypeT type, void * content);

// Append an element
ArgumentListT * appendArgument(ArgumentListT * as, ArgumentListT * a);

// Pretty printing of argument lists
void prettyPrintArgumentList(ArgumentListT * as);

// length
size_t lengthArgumentList(ArgumentListT * as);

// Get an argument as a ssm_memory_t type based on its type
ssm_memory_t getArgumentValue(ArgumentListT * arg);

#endif // __SSM_ASSEMBLER_AST_ARGUMENT_H__
