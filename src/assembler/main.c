#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <limits.h>
#include <sys/stat.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"
#include "interpreter/error.h"

#include "assembler/ast.h"
#include "assembler/parser.h"
#include "assembler/assembler.h"

extern ProgramT * getAST(const char * prog);

static void _usage() {
  fputs("-f <filepath>    File to assemble.\n"
        "-o <outfile>     Output file.\n"
      , stderr);
}

int main(int argc, char ** argv) {

  // Input file, output file and file content buffer.
  char * infile  = NULL
     , * outfile = NULL
     , * fbuf    = NULL
     ;

  // Option for getopt
  char option;

  // For stat, so we can find out the filesize and allocate an array of the
  // correct size.
  struct stat buf;

  // File handle, used for reading and writing to the input and output file
  // respectively.
  FILE * h = NULL;

  // Parsed program
  ProgramT * p = NULL;

  // Assembled program (machine code)
  ssm_memory_t * mc = NULL;

  size_t mcSize = 0;

  // Parse command line options, f and o are required
  while ((option = getopt(argc, argv, "f:o:h")) != -1) {
    switch(option) {
      case 'f':
        infile = strndup(optarg, PATH_MAX); 
        break;

      case 'o':
        outfile = strndup(optarg, PATH_MAX); 
        break;

      case 'h':
        _usage();
        break;

      default:
        _usage();
        ssmError(SSMERR_OPTION_UNKNOWN, "Error: Unknown option: %s\n", option);
        break;
    }
  }

  if (stat(infile, &buf) == -1) {
    perror(NULL);
  } else {

    // Allocate an array to hold the file contents. Open the file in read mode
    // and read the entire contents into fbuf. Close the program afterward.
    fbuf = (char *) malloc(sizeof(char) * buf.st_size);
    h = fopen(infile, "r");
    fread(fbuf, buf.st_size, sizeof(char), h);
    fclose(h);

    // Try to parse the text and assemble the resulting program.
    p  = getAST(fbuf);
    mcSize = assemble(p, &mc);

    // Clean up the program when we are done with it.
    freeProgram(p);

    // Write the resulting machine code to a file.
    h = fopen(outfile, "w");
    for (size_t i = 0; i < mcSize; ++i) {
      fwrite(&mc[i], 1, sizeof(ssm_memory_t), h);
    }
    fclose(h);

    // Clean up the machine code when we are done with it
    free(mc);
  }

  return EXIT_SUCCESS;
}
