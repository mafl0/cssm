/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "src/assembler/parser.y"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"
#include "ast.h"
#include "parser.h"
#include "lexer.h"

void yyerror(ProgramT ** p, yyscan_t scanner, const char * msg) {
  fprintf(stderr, "%s\n", msg); 
}

ProgramT * getAST(const char * prog) {
  ProgramT * p = NULL;
  yyscan_t scanner;
  YY_BUFFER_STATE state;

  // could not initialize
  if (yylex_init(&scanner)) return NULL;
  state = yy_scan_string(prog, scanner);

  yyset_debug(1, scanner); // for debugging

  // Parser error
  if (yyparse(&p, scanner)) return NULL;

  // Clean up
  yy_delete_buffer(state, scanner); 
  yylex_destroy(scanner);

  return p;
}


#line 109 "src/assembler/parser.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "parser.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_T_COLON = 3,                    /* T_COLON  */
  YYSYMBOL_T_ADD = 4,                      /* T_ADD  */
  YYSYMBOL_T_AJS = 5,                      /* T_AJS  */
  YYSYMBOL_T_AND = 6,                      /* T_AND  */
  YYSYMBOL_T_ANNOTE = 7,                   /* T_ANNOTE  */
  YYSYMBOL_T_BRA = 8,                      /* T_BRA  */
  YYSYMBOL_T_BRF = 9,                      /* T_BRF  */
  YYSYMBOL_T_BRT = 10,                     /* T_BRT  */
  YYSYMBOL_T_BSR = 11,                     /* T_BSR  */
  YYSYMBOL_T_DIV = 12,                     /* T_DIV  */
  YYSYMBOL_T_EQ = 13,                      /* T_EQ  */
  YYSYMBOL_T_GE = 14,                      /* T_GE  */
  YYSYMBOL_T_GT = 15,                      /* T_GT  */
  YYSYMBOL_T_HALT = 16,                    /* T_HALT  */
  YYSYMBOL_T_JSR = 17,                     /* T_JSR  */
  YYSYMBOL_T_LDA = 18,                     /* T_LDA  */
  YYSYMBOL_T_LDAA = 19,                    /* T_LDAA  */
  YYSYMBOL_T_LDC = 20,                     /* T_LDC  */
  YYSYMBOL_T_LDH = 21,                     /* T_LDH  */
  YYSYMBOL_T_LDL = 22,                     /* T_LDL  */
  YYSYMBOL_T_LDLA = 23,                    /* T_LDLA  */
  YYSYMBOL_T_LDMA = 24,                    /* T_LDMA  */
  YYSYMBOL_T_LDMH = 25,                    /* T_LDMH  */
  YYSYMBOL_T_LDML = 26,                    /* T_LDML  */
  YYSYMBOL_T_LDMS = 27,                    /* T_LDMS  */
  YYSYMBOL_T_LDR = 28,                     /* T_LDR  */
  YYSYMBOL_T_LDRR = 29,                    /* T_LDRR  */
  YYSYMBOL_T_LDS = 30,                     /* T_LDS  */
  YYSYMBOL_T_LDSA = 31,                    /* T_LDSA  */
  YYSYMBOL_T_LE = 32,                      /* T_LE  */
  YYSYMBOL_T_LINK = 33,                    /* T_LINK  */
  YYSYMBOL_T_LT = 34,                      /* T_LT  */
  YYSYMBOL_T_MOD = 35,                     /* T_MOD  */
  YYSYMBOL_T_MUL = 36,                     /* T_MUL  */
  YYSYMBOL_T_NE = 37,                      /* T_NE  */
  YYSYMBOL_T_NEG = 38,                     /* T_NEG  */
  YYSYMBOL_T_NOP = 39,                     /* T_NOP  */
  YYSYMBOL_T_NOT = 40,                     /* T_NOT  */
  YYSYMBOL_T_OR = 41,                      /* T_OR  */
  YYSYMBOL_T_RET = 42,                     /* T_RET  */
  YYSYMBOL_T_STA = 43,                     /* T_STA  */
  YYSYMBOL_T_STH = 44,                     /* T_STH  */
  YYSYMBOL_T_STL = 45,                     /* T_STL  */
  YYSYMBOL_T_STMA = 46,                    /* T_STMA  */
  YYSYMBOL_T_STMH = 47,                    /* T_STMH  */
  YYSYMBOL_T_STML = 48,                    /* T_STML  */
  YYSYMBOL_T_STMS = 49,                    /* T_STMS  */
  YYSYMBOL_T_STR = 50,                     /* T_STR  */
  YYSYMBOL_T_STS = 51,                     /* T_STS  */
  YYSYMBOL_T_SUB = 52,                     /* T_SUB  */
  YYSYMBOL_T_SWP = 53,                     /* T_SWP  */
  YYSYMBOL_T_SWPR = 54,                    /* T_SWPR  */
  YYSYMBOL_T_SWPRR = 55,                   /* T_SWPRR  */
  YYSYMBOL_T_TRAP = 56,                    /* T_TRAP  */
  YYSYMBOL_T_UNLINK = 57,                  /* T_UNLINK  */
  YYSYMBOL_T_XOR = 58,                     /* T_XOR  */
  YYSYMBOL_T_PC = 59,                      /* T_PC  */
  YYSYMBOL_T_HP = 60,                      /* T_HP  */
  YYSYMBOL_T_MP = 61,                      /* T_MP  */
  YYSYMBOL_T_R0 = 62,                      /* T_R0  */
  YYSYMBOL_T_R1 = 63,                      /* T_R1  */
  YYSYMBOL_T_R2 = 64,                      /* T_R2  */
  YYSYMBOL_T_R3 = 65,                      /* T_R3  */
  YYSYMBOL_T_R4 = 66,                      /* T_R4  */
  YYSYMBOL_T_R5 = 67,                      /* T_R5  */
  YYSYMBOL_T_R6 = 68,                      /* T_R6  */
  YYSYMBOL_T_R7 = 69,                      /* T_R7  */
  YYSYMBOL_T_RR = 70,                      /* T_RR  */
  YYSYMBOL_T_SP = 71,                      /* T_SP  */
  YYSYMBOL_T_SR = 72,                      /* T_SR  */
  YYSYMBOL_T_CONSTANT = 73,                /* T_CONSTANT  */
  YYSYMBOL_T_LABEL = 74,                   /* T_LABEL  */
  YYSYMBOL_YYACCEPT = 75,                  /* $accept  */
  YYSYMBOL_input = 76,                     /* input  */
  YYSYMBOL_program = 77,                   /* program  */
  YYSYMBOL_node = 78,                      /* node  */
  YYSYMBOL_instruction = 79,               /* instruction  */
  YYSYMBOL_register = 80,                  /* register  */
  YYSYMBOL_address = 81,                   /* address  */
  YYSYMBOL_constant = 82                   /* constant  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
# define YYCOPY_NEEDED 1
#endif /* 1 */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  132
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   203

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  75
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  8
/* YYNRULES -- Number of rules.  */
#define YYNRULES  116
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  168

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   329


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   151,   151,   154,   155,   160,   161,   165,   167,   168,
     170,   173,   174,   176,   177,   179,   180,   183,   184,   186,
     187,   188,   189,   190,   191,   193,   194,   196,   197,   199,
     200,   202,   203,   205,   206,   208,   209,   211,   212,   213,
     214,   216,   217,   218,   219,   221,   222,   223,   224,   226,
     227,   228,   229,   231,   232,   234,   235,   237,   238,   240,
     241,   242,   243,   244,   245,   246,   247,   248,   249,   250,
     252,   253,   255,   257,   258,   260,   261,   262,   263,   265,
     266,   267,   268,   270,   271,   272,   273,   275,   276,   277,
     278,   280,   282,   283,   285,   286,   288,   289,   291,   292,
     293,   297,   298,   299,   300,   301,   302,   303,   304,   305,
     306,   307,   308,   309,   310,   315,   320
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  static const char *const yy_sname[] =
  {
  "end of file", "error", "invalid token", "T_COLON", "T_ADD", "T_AJS",
  "T_AND", "T_ANNOTE", "T_BRA", "T_BRF", "T_BRT", "T_BSR", "T_DIV", "T_EQ",
  "T_GE", "T_GT", "T_HALT", "T_JSR", "T_LDA", "T_LDAA", "T_LDC", "T_LDH",
  "T_LDL", "T_LDLA", "T_LDMA", "T_LDMH", "T_LDML", "T_LDMS", "T_LDR",
  "T_LDRR", "T_LDS", "T_LDSA", "T_LE", "T_LINK", "T_LT", "T_MOD", "T_MUL",
  "T_NE", "T_NEG", "T_NOP", "T_NOT", "T_OR", "T_RET", "T_STA", "T_STH",
  "T_STL", "T_STMA", "T_STMH", "T_STML", "T_STMS", "T_STR", "T_STS",
  "T_SUB", "T_SWP", "T_SWPR", "T_SWPRR", "T_TRAP", "T_UNLINK", "T_XOR",
  "T_PC", "T_HP", "T_MP", "T_R0", "T_R1", "T_R2", "T_R3", "T_R4", "T_R5",
  "T_R6", "T_R7", "T_RR", "T_SP", "T_SR", "T_CONSTANT", "T_LABEL",
  "$accept", "input", "program", "node", "instruction", "register",
  "address", "constant", YY_NULLPTR
  };
  return yy_sname[yysymbol];
}
#endif

#define YYPACT_NINF (-64)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     111,   -64,   -63,   -64,   -54,   -52,   -50,   -37,   -64,   -64,
     -64,   -64,   -64,   -64,   -17,   -15,   -13,   -10,    -8,    -6,
       5,     7,     9,    12,   -18,   -18,    14,    16,   -64,   -47,
     -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,    18,
     -64,    20,    24,    26,    28,    38,   -18,    40,   -64,   -64,
     -18,   -18,   -47,   -64,   -64,    29,    34,   -64,   111,   -64,
     -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,
     -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,
     -64,   -64,   -64,   -64,    97,    99,   101,   103,   105,   107,
     109,   113,   -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,
     -64,   -64,   -64,   -64,   -64,   -64,   -64,   -18,   -64,   -64,
     -64,   -64,   -64,   -64,   -64,   -64,   -64,   115,   117,   119,
     121,   123,   125,   127,   129,   -64,   -64,   -64,   -64,   -18,
     -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,
     -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,
     -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64,
     -64,   -64,   -64,   -64,   -64,   -64,   -64,   -64
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       4,     7,     0,    10,     0,     0,     0,     0,    19,    20,
      21,    22,    23,    24,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    59,     0,
      61,    62,    63,    64,    65,    66,    67,    68,    69,     0,
      72,     0,     0,     0,     0,     0,     0,     0,    94,    95,
       0,     0,     0,    99,   100,     0,     0,     2,     4,     5,
     116,     9,     8,   115,    11,    12,    13,    14,    15,    16,
      17,    18,    28,    27,    26,    25,    30,    29,    32,    31,
      36,    35,    34,    33,     0,     0,     0,     0,     0,     0,
       0,     0,   109,   112,   111,   101,   102,   103,   104,   105,
     106,   107,   108,   113,   110,   114,    54,     0,    56,    55,
      58,    57,    60,    71,    70,    74,    73,     0,     0,     0,
       0,     0,     0,     0,     0,    91,    93,    92,    97,     0,
      98,     6,     1,     3,    40,    38,    39,    37,    44,    42,
      43,    41,    48,    46,    47,    45,    52,    50,    51,    49,
      53,    78,    76,    77,    75,    82,    80,    81,    79,    86,
      84,    85,    83,    90,    88,    89,    87,    96
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -64,   -64,    -3,   -64,   -64,   -11,    11,   -14
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
       0,    56,    57,    58,    59,   106,    65,    62
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      73,    75,    77,    79,    81,    83,    85,    87,    89,    91,
      60,    61,   109,   111,   107,   112,    67,    69,    71,    63,
      64,    63,    66,    63,    68,   114,    60,   116,   118,   120,
     122,   124,   131,   127,   132,   125,    63,    70,   130,   128,
     129,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   133,    60,    72,    60,    74,
      60,    76,     0,    60,    78,    60,    80,    60,    82,     0,
     135,   137,   139,   141,   143,   145,   147,   149,    60,    84,
      60,    86,    60,    88,     0,    60,    90,    60,   108,    60,
     110,    60,   113,    60,   115,     0,   150,    60,   117,    60,
     119,    60,   121,   152,   154,   156,   158,   160,   162,   164,
     166,    60,   123,    60,   126,     1,     2,     3,   167,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      60,   134,    60,   136,    60,   138,    60,   140,    60,   142,
      60,   144,    60,   146,     0,    55,    60,   148,    60,   151,
      60,   153,    60,   155,    60,   157,    60,   159,    60,   161,
      60,   163,    60,   165
};

static const yytype_int16 yycheck[] =
{
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      73,    74,    26,    27,    25,    29,     5,     6,     7,    73,
      74,    73,    74,    73,    74,    39,    73,    41,    42,    43,
      44,    45,     3,    47,     0,    46,    73,    74,    52,    50,
      51,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    58,    73,    74,    73,    74,
      73,    74,    -1,    73,    74,    73,    74,    73,    74,    -1,
      84,    85,    86,    87,    88,    89,    90,    91,    73,    74,
      73,    74,    73,    74,    -1,    73,    74,    73,    74,    73,
      74,    73,    74,    73,    74,    -1,   107,    73,    74,    73,
      74,    73,    74,   117,   118,   119,   120,   121,   122,   123,
     124,    73,    74,    73,    74,     4,     5,     6,   129,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      73,    74,    73,    74,    73,    74,    73,    74,    73,    74,
      73,    74,    73,    74,    -1,    74,    73,    74,    73,    74,
      73,    74,    73,    74,    73,    74,    73,    74,    73,    74,
      73,    74,    73,    74
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,     4,     5,     6,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    74,    76,    77,    78,    79,
      73,    74,    82,    73,    74,    81,    74,    81,    74,    81,
      74,    81,    74,    82,    74,    82,    74,    82,    74,    82,
      74,    82,    74,    82,    74,    82,    74,    82,    74,    82,
      74,    82,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    80,    80,    74,    82,
      74,    82,    82,    74,    82,    74,    82,    74,    82,    74,
      82,    74,    82,    74,    82,    80,    74,    82,    80,    80,
      82,     3,     0,    77,    74,    82,    74,    82,    74,    82,
      74,    82,    74,    82,    74,    82,    74,    82,    74,    82,
      80,    74,    82,    74,    82,    74,    82,    74,    82,    74,
      82,    74,    82,    74,    82,    74,    82,    80
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    75,    76,    77,    77,    78,    78,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    80,    80,    80,    80,    80,    80,    80,    80,    80,
      80,    80,    80,    80,    80,    81,    82
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     2,     0,     1,     2,     1,     2,     2,
       1,     2,     2,     2,     2,     2,     2,     2,     2,     1,
       1,     1,     1,     1,     1,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     2,     2,     2,     2,     2,     1,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       2,     2,     1,     2,     2,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     2,     2,     2,     1,     1,     3,     2,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        YY_LAC_DISCARD ("YYBACKUP");                              \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (program, scanner, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value, program, scanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, ProgramT ** program, void * scanner)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  YY_USE (program);
  YY_USE (scanner);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep, ProgramT ** program, void * scanner)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep, program, scanner);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule, ProgramT ** program, void * scanner)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)], program, scanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule, program, scanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Given a state stack such that *YYBOTTOM is its bottom, such that
   *YYTOP is either its top or is YYTOP_EMPTY to indicate an empty
   stack, and such that *YYCAPACITY is the maximum number of elements it
   can hold without a reallocation, make sure there is enough room to
   store YYADD more elements.  If not, allocate a new stack using
   YYSTACK_ALLOC, copy the existing elements, and adjust *YYBOTTOM,
   *YYTOP, and *YYCAPACITY to reflect the new capacity and memory
   location.  If *YYBOTTOM != YYBOTTOM_NO_FREE, then free the old stack
   using YYSTACK_FREE.  Return 0 if successful or if no reallocation is
   required.  Return YYENOMEM if memory is exhausted.  */
static int
yy_lac_stack_realloc (YYPTRDIFF_T *yycapacity, YYPTRDIFF_T yyadd,
#if YYDEBUG
                      char const *yydebug_prefix,
                      char const *yydebug_suffix,
#endif
                      yy_state_t **yybottom,
                      yy_state_t *yybottom_no_free,
                      yy_state_t **yytop, yy_state_t *yytop_empty)
{
  YYPTRDIFF_T yysize_old =
    *yytop == yytop_empty ? 0 : *yytop - *yybottom + 1;
  YYPTRDIFF_T yysize_new = yysize_old + yyadd;
  if (*yycapacity < yysize_new)
    {
      YYPTRDIFF_T yyalloc = 2 * yysize_new;
      yy_state_t *yybottom_new;
      /* Use YYMAXDEPTH for maximum stack size given that the stack
         should never need to grow larger than the main state stack
         needs to grow without LAC.  */
      if (YYMAXDEPTH < yysize_new)
        {
          YYDPRINTF ((stderr, "%smax size exceeded%s", yydebug_prefix,
                      yydebug_suffix));
          return YYENOMEM;
        }
      if (YYMAXDEPTH < yyalloc)
        yyalloc = YYMAXDEPTH;
      yybottom_new =
        YY_CAST (yy_state_t *,
                 YYSTACK_ALLOC (YY_CAST (YYSIZE_T,
                                         yyalloc * YYSIZEOF (*yybottom_new))));
      if (!yybottom_new)
        {
          YYDPRINTF ((stderr, "%srealloc failed%s", yydebug_prefix,
                      yydebug_suffix));
          return YYENOMEM;
        }
      if (*yytop != yytop_empty)
        {
          YYCOPY (yybottom_new, *yybottom, yysize_old);
          *yytop = yybottom_new + (yysize_old - 1);
        }
      if (*yybottom != yybottom_no_free)
        YYSTACK_FREE (*yybottom);
      *yybottom = yybottom_new;
      *yycapacity = yyalloc;
    }
  return 0;
}

/* Establish the initial context for the current lookahead if no initial
   context is currently established.

   We define a context as a snapshot of the parser stacks.  We define
   the initial context for a lookahead as the context in which the
   parser initially examines that lookahead in order to select a
   syntactic action.  Thus, if the lookahead eventually proves
   syntactically unacceptable (possibly in a later context reached via a
   series of reductions), the initial context can be used to determine
   the exact set of tokens that would be syntactically acceptable in the
   lookahead's place.  Moreover, it is the context after which any
   further semantic actions would be erroneous because they would be
   determined by a syntactically unacceptable token.

   YY_LAC_ESTABLISH should be invoked when a reduction is about to be
   performed in an inconsistent state (which, for the purposes of LAC,
   includes consistent states that don't know they're consistent because
   their default reductions have been disabled).  Iff there is a
   lookahead token, it should also be invoked before reporting a syntax
   error.  This latter case is for the sake of the debugging output.

   For parse.lac=full, the implementation of YY_LAC_ESTABLISH is as
   follows.  If no initial context is currently established for the
   current lookahead, then check if that lookahead can eventually be
   shifted if syntactic actions continue from the current context.
   Report a syntax error if it cannot.  */
#define YY_LAC_ESTABLISH                                                \
do {                                                                    \
  if (!yy_lac_established)                                              \
    {                                                                   \
      YYDPRINTF ((stderr,                                               \
                  "LAC: initial context established for %s\n",          \
                  yysymbol_name (yytoken)));                            \
      yy_lac_established = 1;                                           \
      switch (yy_lac (yyesa, &yyes, &yyes_capacity, yyssp, yytoken))    \
        {                                                               \
        case YYENOMEM:                                                  \
          YYNOMEM;                                                      \
        case 1:                                                         \
          goto yyerrlab;                                                \
        }                                                               \
    }                                                                   \
} while (0)

/* Discard any previous initial lookahead context because of Event,
   which may be a lookahead change or an invalidation of the currently
   established initial context for the current lookahead.

   The most common example of a lookahead change is a shift.  An example
   of both cases is syntax error recovery.  That is, a syntax error
   occurs when the lookahead is syntactically erroneous for the
   currently established initial context, so error recovery manipulates
   the parser stacks to try to find a new initial context in which the
   current lookahead is syntactically acceptable.  If it fails to find
   such a context, it discards the lookahead.  */
#if YYDEBUG
# define YY_LAC_DISCARD(Event)                                           \
do {                                                                     \
  if (yy_lac_established)                                                \
    {                                                                    \
      YYDPRINTF ((stderr, "LAC: initial context discarded due to "       \
                  Event "\n"));                                          \
      yy_lac_established = 0;                                            \
    }                                                                    \
} while (0)
#else
# define YY_LAC_DISCARD(Event) yy_lac_established = 0
#endif

/* Given the stack whose top is *YYSSP, return 0 iff YYTOKEN can
   eventually (after perhaps some reductions) be shifted, return 1 if
   not, or return YYENOMEM if memory is exhausted.  As preconditions and
   postconditions: *YYES_CAPACITY is the allocated size of the array to
   which *YYES points, and either *YYES = YYESA or *YYES points to an
   array allocated with YYSTACK_ALLOC.  yy_lac may overwrite the
   contents of either array, alter *YYES and *YYES_CAPACITY, and free
   any old *YYES other than YYESA.  */
static int
yy_lac (yy_state_t *yyesa, yy_state_t **yyes,
        YYPTRDIFF_T *yyes_capacity, yy_state_t *yyssp, yysymbol_kind_t yytoken)
{
  yy_state_t *yyes_prev = yyssp;
  yy_state_t *yyesp = yyes_prev;
  /* Reduce until we encounter a shift and thereby accept the token.  */
  YYDPRINTF ((stderr, "LAC: checking lookahead %s:", yysymbol_name (yytoken)));
  if (yytoken == YYSYMBOL_YYUNDEF)
    {
      YYDPRINTF ((stderr, " Always Err\n"));
      return 1;
    }
  while (1)
    {
      int yyrule = yypact[+*yyesp];
      if (yypact_value_is_default (yyrule)
          || (yyrule += yytoken) < 0 || YYLAST < yyrule
          || yycheck[yyrule] != yytoken)
        {
          /* Use the default action.  */
          yyrule = yydefact[+*yyesp];
          if (yyrule == 0)
            {
              YYDPRINTF ((stderr, " Err\n"));
              return 1;
            }
        }
      else
        {
          /* Use the action from yytable.  */
          yyrule = yytable[yyrule];
          if (yytable_value_is_error (yyrule))
            {
              YYDPRINTF ((stderr, " Err\n"));
              return 1;
            }
          if (0 < yyrule)
            {
              YYDPRINTF ((stderr, " S%d\n", yyrule));
              return 0;
            }
          yyrule = -yyrule;
        }
      /* By now we know we have to simulate a reduce.  */
      YYDPRINTF ((stderr, " R%d", yyrule - 1));
      {
        /* Pop the corresponding number of values from the stack.  */
        YYPTRDIFF_T yylen = yyr2[yyrule];
        /* First pop from the LAC stack as many tokens as possible.  */
        if (yyesp != yyes_prev)
          {
            YYPTRDIFF_T yysize = yyesp - *yyes + 1;
            if (yylen < yysize)
              {
                yyesp -= yylen;
                yylen = 0;
              }
            else
              {
                yyesp = yyes_prev;
                yylen -= yysize;
              }
          }
        /* Only afterwards look at the main stack.  */
        if (yylen)
          yyesp = yyes_prev -= yylen;
      }
      /* Push the resulting state of the reduction.  */
      {
        yy_state_fast_t yystate;
        {
          const int yylhs = yyr1[yyrule] - YYNTOKENS;
          const int yyi = yypgoto[yylhs] + *yyesp;
          yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyesp
                     ? yytable[yyi]
                     : yydefgoto[yylhs]);
        }
        if (yyesp == yyes_prev)
          {
            yyesp = *yyes;
            YY_IGNORE_USELESS_CAST_BEGIN
            *yyesp = YY_CAST (yy_state_t, yystate);
            YY_IGNORE_USELESS_CAST_END
          }
        else
          {
            if (yy_lac_stack_realloc (yyes_capacity, 1,
#if YYDEBUG
                                      " (", ")",
#endif
                                      yyes, yyesa, &yyesp, yyes_prev))
              {
                YYDPRINTF ((stderr, "\n"));
                return YYENOMEM;
              }
            YY_IGNORE_USELESS_CAST_BEGIN
            *++yyesp = YY_CAST (yy_state_t, yystate);
            YY_IGNORE_USELESS_CAST_END
          }
        YYDPRINTF ((stderr, " G%d", yystate));
      }
    }
}

/* Context of a parse error.  */
typedef struct
{
  yy_state_t *yyssp;
  yy_state_t *yyesa;
  yy_state_t **yyes;
  YYPTRDIFF_T *yyes_capacity;
  yysymbol_kind_t yytoken;
} yypcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
yypcontext_expected_tokens (const yypcontext_t *yyctx,
                            yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;

  int yyx;
  for (yyx = 0; yyx < YYNTOKENS; ++yyx)
    {
      yysymbol_kind_t yysym = YY_CAST (yysymbol_kind_t, yyx);
      if (yysym != YYSYMBOL_YYerror && yysym != YYSYMBOL_YYUNDEF)
        switch (yy_lac (yyctx->yyesa, yyctx->yyes, yyctx->yyes_capacity, yyctx->yyssp, yysym))
          {
          case YYENOMEM:
            return YYENOMEM;
          case 1:
            continue;
          default:
            if (!yyarg)
              ++yycount;
            else if (yycount == yyargn)
              return 0;
            else
              yyarg[yycount++] = yysym;
          }
    }
  if (yyarg && yycount == 0 && 0 < yyargn)
    yyarg[0] = YYSYMBOL_YYEMPTY;
  return yycount;
}




#ifndef yystrlen
# if defined __GLIBC__ && defined _STRING_H
#  define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
# else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
# endif
#endif

#ifndef yystpcpy
# if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#  define yystpcpy stpcpy
# else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
# endif
#endif



static int
yy_syntax_error_arguments (const yypcontext_t *yyctx,
                           yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;
  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
       In the first two cases, it might appear that the current syntax
       error should have been detected in the previous state when yy_lac
       was invoked.  However, at that time, there might have been a
       different syntax error that discarded a different initial context
       during error recovery, leaving behind the current lookahead.
  */
  if (yyctx->yytoken != YYSYMBOL_YYEMPTY)
    {
      int yyn;
      YYDPRINTF ((stderr, "Constructing syntax error message\n"));
      if (yyarg)
        yyarg[yycount] = yyctx->yytoken;
      ++yycount;
      yyn = yypcontext_expected_tokens (yyctx,
                                        yyarg ? yyarg + 1 : yyarg, yyargn - 1);
      if (yyn == YYENOMEM)
        return YYENOMEM;
      else if (yyn == 0)
        YYDPRINTF ((stderr, "No expected tokens.\n"));
      else
        yycount += yyn;
    }
  return yycount;
}

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.  In order to see if a particular token T is a
   valid looakhead, invoke yy_lac (YYESA, YYES, YYES_CAPACITY, YYSSP, T).

   Return 0 if *YYMSG was successfully written.  Return -1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return YYENOMEM if the
   required number of bytes is too large to store or if
   yy_lac returned YYENOMEM.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                const yypcontext_t *yyctx)
{
  enum { YYARGS_MAX = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  yysymbol_kind_t yyarg[YYARGS_MAX];
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* Actual size of YYARG. */
  int yycount = yy_syntax_error_arguments (yyctx, yyarg, YYARGS_MAX);
  if (yycount == YYENOMEM)
    return YYENOMEM;

  switch (yycount)
    {
#define YYCASE_(N, S)                       \
      case N:                               \
        yyformat = S;                       \
        break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
    }

  /* Compute error message size.  Don't count the "%s"s, but reserve
     room for the terminator.  */
  yysize = yystrlen (yyformat) - 2 * yycount + 1;
  {
    int yyi;
    for (yyi = 0; yyi < yycount; ++yyi)
      {
        YYPTRDIFF_T yysize1
          = yysize + yystrlen (yysymbol_name (yyarg[yyi]));
        if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
          yysize = yysize1;
        else
          return YYENOMEM;
      }
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return -1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp = yystpcpy (yyp, yysymbol_name (yyarg[yyi++]));
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep, ProgramT ** program, void * scanner)
{
  YY_USE (yyvaluep);
  YY_USE (program);
  YY_USE (scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}






/*----------.
| yyparse.  |
`----------*/

int
yyparse (ProgramT ** program, void * scanner)
{
/* Lookahead token kind.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs = 0;

    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

    yy_state_t yyesa[20];
    yy_state_t *yyes = yyesa;
    YYPTRDIFF_T yyes_capacity = 20 < YYMAXDEPTH ? 20 : YYMAXDEPTH;

  /* Whether LAC context is established.  A Boolean.  */
  int yy_lac_established = 0;
  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex (&yylval, scanner);
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    {
      YY_LAC_ESTABLISH;
      goto yydefault;
    }
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      YY_LAC_ESTABLISH;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  YY_LAC_DISCARD ("shift");
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  {
    int yychar_backup = yychar;
    switch (yyn)
      {
  case 2: /* input: program  */
#line 151 "src/assembler/parser.y"
                              { *program = (yyvsp[0].program); }
#line 1767 "src/assembler/parser.c"
    break;

  case 3: /* program: node program  */
#line 154 "src/assembler/parser.y"
                              { (yyval.program) = consProgram((yyvsp[-1].node), (yyvsp[0].program)); }
#line 1773 "src/assembler/parser.c"
    break;

  case 4: /* program: %empty  */
#line 155 "src/assembler/parser.y"
                              { (yyval.program) = NULL; }
#line 1779 "src/assembler/parser.c"
    break;

  case 5: /* node: instruction  */
#line 160 "src/assembler/parser.y"
                              { (yyval.node) = (yyvsp[0].node); }
#line 1785 "src/assembler/parser.c"
    break;

  case 6: /* node: T_LABEL T_COLON  */
#line 161 "src/assembler/parser.y"
                              { (yyval.node) = createLabel((yyvsp[-1].ssmLabel)); }
#line 1791 "src/assembler/parser.c"
    break;

  case 7: /* instruction: T_ADD  */
#line 165 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_ADD);                                                }
#line 1797 "src/assembler/parser.c"
    break;

  case 8: /* instruction: T_AJS constant  */
#line 167 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_AJS,   ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 1803 "src/assembler/parser.c"
    break;

  case 9: /* instruction: T_AJS T_LABEL  */
#line 168 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_AJS,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 1809 "src/assembler/parser.c"
    break;

  case 10: /* instruction: T_AND  */
#line 170 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_AND);                                                }
#line 1815 "src/assembler/parser.c"
    break;

  case 11: /* instruction: T_BRA T_LABEL  */
#line 173 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_BRA,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 1821 "src/assembler/parser.c"
    break;

  case 12: /* instruction: T_BRA address  */
#line 174 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_BRA,   AddressArgument,  (yyvsp[0].ssmPtr));                        }
#line 1827 "src/assembler/parser.c"
    break;

  case 13: /* instruction: T_BRF T_LABEL  */
#line 176 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_BRF,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 1833 "src/assembler/parser.c"
    break;

  case 14: /* instruction: T_BRF address  */
#line 177 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_BRF,   AddressArgument,  (yyvsp[0].ssmPtr));                        }
#line 1839 "src/assembler/parser.c"
    break;

  case 15: /* instruction: T_BRT T_LABEL  */
#line 179 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_BRT,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 1845 "src/assembler/parser.c"
    break;

  case 16: /* instruction: T_BRT address  */
#line 180 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_BRT,   AddressArgument,  (yyvsp[0].ssmPtr));                        }
#line 1851 "src/assembler/parser.c"
    break;

  case 17: /* instruction: T_BSR T_LABEL  */
#line 183 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_BSR,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 1857 "src/assembler/parser.c"
    break;

  case 18: /* instruction: T_BSR address  */
#line 184 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_BSR,   AddressArgument,  (yyvsp[0].ssmPtr));                        }
#line 1863 "src/assembler/parser.c"
    break;

  case 19: /* instruction: T_DIV  */
#line 186 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_DIV);                                                }
#line 1869 "src/assembler/parser.c"
    break;

  case 20: /* instruction: T_EQ  */
#line 187 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_EQ);                                                 }
#line 1875 "src/assembler/parser.c"
    break;

  case 21: /* instruction: T_GE  */
#line 188 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_GE);                                                 }
#line 1881 "src/assembler/parser.c"
    break;

  case 22: /* instruction: T_GT  */
#line 189 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_GT);                                                 }
#line 1887 "src/assembler/parser.c"
    break;

  case 23: /* instruction: T_HALT  */
#line 190 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_HALT);                                               }
#line 1893 "src/assembler/parser.c"
    break;

  case 24: /* instruction: T_JSR  */
#line 191 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_JSR);                                                }
#line 1899 "src/assembler/parser.c"
    break;

  case 25: /* instruction: T_LDAA constant  */
#line 193 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDAA,  ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 1905 "src/assembler/parser.c"
    break;

  case 26: /* instruction: T_LDAA T_LABEL  */
#line 194 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDAA,  LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 1911 "src/assembler/parser.c"
    break;

  case 27: /* instruction: T_LDA constant  */
#line 196 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDA,   ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 1917 "src/assembler/parser.c"
    break;

  case 28: /* instruction: T_LDA T_LABEL  */
#line 197 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDA,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 1923 "src/assembler/parser.c"
    break;

  case 29: /* instruction: T_LDC constant  */
#line 199 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDC,   ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 1929 "src/assembler/parser.c"
    break;

  case 30: /* instruction: T_LDC T_LABEL  */
#line 200 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDC,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 1935 "src/assembler/parser.c"
    break;

  case 31: /* instruction: T_LDH constant  */
#line 202 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDH,   ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 1941 "src/assembler/parser.c"
    break;

  case 32: /* instruction: T_LDH T_LABEL  */
#line 203 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDH,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 1947 "src/assembler/parser.c"
    break;

  case 33: /* instruction: T_LDLA constant  */
#line 205 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDLA,  ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 1953 "src/assembler/parser.c"
    break;

  case 34: /* instruction: T_LDLA T_LABEL  */
#line 206 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDLA,  LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 1959 "src/assembler/parser.c"
    break;

  case 35: /* instruction: T_LDL constant  */
#line 208 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDL,   ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 1965 "src/assembler/parser.c"
    break;

  case 36: /* instruction: T_LDL T_LABEL  */
#line 209 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDL,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 1971 "src/assembler/parser.c"
    break;

  case 37: /* instruction: T_LDMA constant constant  */
#line 211 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDMA,  ValueArgument,    (yyvsp[-1].ssmVal), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 1977 "src/assembler/parser.c"
    break;

  case 38: /* instruction: T_LDMA T_LABEL constant  */
#line 212 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDMA,  LabelArgument,    (yyvsp[-1].ssmLabel), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 1983 "src/assembler/parser.c"
    break;

  case 39: /* instruction: T_LDMA constant T_LABEL  */
#line 213 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDMA,  ValueArgument,    (yyvsp[-1].ssmVal), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 1989 "src/assembler/parser.c"
    break;

  case 40: /* instruction: T_LDMA T_LABEL T_LABEL  */
#line 214 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDMA,  LabelArgument,    (yyvsp[-1].ssmLabel), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 1995 "src/assembler/parser.c"
    break;

  case 41: /* instruction: T_LDMH constant constant  */
#line 216 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDMH,  ValueArgument,    (yyvsp[-1].ssmVal), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2001 "src/assembler/parser.c"
    break;

  case 42: /* instruction: T_LDMH T_LABEL constant  */
#line 217 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDMH,  LabelArgument,    (yyvsp[-1].ssmLabel), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2007 "src/assembler/parser.c"
    break;

  case 43: /* instruction: T_LDMH constant T_LABEL  */
#line 218 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDMH,  ValueArgument,    (yyvsp[-1].ssmVal), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2013 "src/assembler/parser.c"
    break;

  case 44: /* instruction: T_LDMH T_LABEL T_LABEL  */
#line 219 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDMH,  LabelArgument,    (yyvsp[-1].ssmLabel), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2019 "src/assembler/parser.c"
    break;

  case 45: /* instruction: T_LDML constant constant  */
#line 221 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDML,  ValueArgument,    (yyvsp[-1].ssmVal), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2025 "src/assembler/parser.c"
    break;

  case 46: /* instruction: T_LDML T_LABEL constant  */
#line 222 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDML,  LabelArgument,    (yyvsp[-1].ssmLabel), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2031 "src/assembler/parser.c"
    break;

  case 47: /* instruction: T_LDML constant T_LABEL  */
#line 223 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDML,  ValueArgument,    (yyvsp[-1].ssmVal), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2037 "src/assembler/parser.c"
    break;

  case 48: /* instruction: T_LDML T_LABEL T_LABEL  */
#line 224 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDML,  LabelArgument,    (yyvsp[-1].ssmLabel), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2043 "src/assembler/parser.c"
    break;

  case 49: /* instruction: T_LDMS constant constant  */
#line 226 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDMS,  ValueArgument,    (yyvsp[-1].ssmVal), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2049 "src/assembler/parser.c"
    break;

  case 50: /* instruction: T_LDMS T_LABEL constant  */
#line 227 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDMS,  LabelArgument,    (yyvsp[-1].ssmLabel), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2055 "src/assembler/parser.c"
    break;

  case 51: /* instruction: T_LDMS constant T_LABEL  */
#line 228 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDMS,  ValueArgument,    (yyvsp[-1].ssmVal), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2061 "src/assembler/parser.c"
    break;

  case 52: /* instruction: T_LDMS T_LABEL T_LABEL  */
#line 229 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDMS,  LabelArgument,    (yyvsp[-1].ssmLabel), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2067 "src/assembler/parser.c"
    break;

  case 53: /* instruction: T_LDRR register register  */
#line 231 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDRR,  RegisterArgument, (yyvsp[-1].ssmReg), RegisterArgument, (yyvsp[0].ssmReg));  }
#line 2073 "src/assembler/parser.c"
    break;

  case 54: /* instruction: T_LDR register  */
#line 232 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDR,   RegisterArgument, (yyvsp[0].ssmReg));                        }
#line 2079 "src/assembler/parser.c"
    break;

  case 55: /* instruction: T_LDS constant  */
#line 234 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDS,   ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 2085 "src/assembler/parser.c"
    break;

  case 56: /* instruction: T_LDS T_LABEL  */
#line 235 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDS,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 2091 "src/assembler/parser.c"
    break;

  case 57: /* instruction: T_LDSA constant  */
#line 237 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDSA,  ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 2097 "src/assembler/parser.c"
    break;

  case 58: /* instruction: T_LDSA T_LABEL  */
#line 238 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LDSA,  LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 2103 "src/assembler/parser.c"
    break;

  case 59: /* instruction: T_LE  */
#line 240 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LE);                                                 }
#line 2109 "src/assembler/parser.c"
    break;

  case 60: /* instruction: T_LINK constant  */
#line 241 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LINK,  ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 2115 "src/assembler/parser.c"
    break;

  case 61: /* instruction: T_LT  */
#line 242 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_LT);                                                 }
#line 2121 "src/assembler/parser.c"
    break;

  case 62: /* instruction: T_MOD  */
#line 243 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_MOD);                                                }
#line 2127 "src/assembler/parser.c"
    break;

  case 63: /* instruction: T_MUL  */
#line 244 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_MUL);                                                }
#line 2133 "src/assembler/parser.c"
    break;

  case 64: /* instruction: T_NE  */
#line 245 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_NE);                                                 }
#line 2139 "src/assembler/parser.c"
    break;

  case 65: /* instruction: T_NEG  */
#line 246 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_NEG);                                                }
#line 2145 "src/assembler/parser.c"
    break;

  case 66: /* instruction: T_NOP  */
#line 247 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_NOP);                                                }
#line 2151 "src/assembler/parser.c"
    break;

  case 67: /* instruction: T_NOT  */
#line 248 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_NOT);                                                }
#line 2157 "src/assembler/parser.c"
    break;

  case 68: /* instruction: T_OR  */
#line 249 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_OR);                                                 }
#line 2163 "src/assembler/parser.c"
    break;

  case 69: /* instruction: T_RET  */
#line 250 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_RET);                                                }
#line 2169 "src/assembler/parser.c"
    break;

  case 70: /* instruction: T_STA constant  */
#line 252 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STA,   ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 2175 "src/assembler/parser.c"
    break;

  case 71: /* instruction: T_STA T_LABEL  */
#line 253 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STA,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 2181 "src/assembler/parser.c"
    break;

  case 72: /* instruction: T_STH  */
#line 255 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STH);                                                }
#line 2187 "src/assembler/parser.c"
    break;

  case 73: /* instruction: T_STL constant  */
#line 257 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STL,   ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 2193 "src/assembler/parser.c"
    break;

  case 74: /* instruction: T_STL T_LABEL  */
#line 258 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STL,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 2199 "src/assembler/parser.c"
    break;

  case 75: /* instruction: T_STMA constant constant  */
#line 260 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STMA,  ValueArgument,    (yyvsp[-1].ssmVal), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2205 "src/assembler/parser.c"
    break;

  case 76: /* instruction: T_STMA T_LABEL constant  */
#line 261 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STMA,  LabelArgument,    (yyvsp[-1].ssmLabel), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2211 "src/assembler/parser.c"
    break;

  case 77: /* instruction: T_STMA constant T_LABEL  */
#line 262 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STMA,  ValueArgument,    (yyvsp[-1].ssmVal), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2217 "src/assembler/parser.c"
    break;

  case 78: /* instruction: T_STMA T_LABEL T_LABEL  */
#line 263 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STMA,  LabelArgument,    (yyvsp[-1].ssmLabel), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2223 "src/assembler/parser.c"
    break;

  case 79: /* instruction: T_STMH constant constant  */
#line 265 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STMH,  ValueArgument,    (yyvsp[-1].ssmVal), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2229 "src/assembler/parser.c"
    break;

  case 80: /* instruction: T_STMH T_LABEL constant  */
#line 266 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STMH,  LabelArgument,    (yyvsp[-1].ssmLabel), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2235 "src/assembler/parser.c"
    break;

  case 81: /* instruction: T_STMH constant T_LABEL  */
#line 267 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STMH,  ValueArgument,    (yyvsp[-1].ssmVal), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2241 "src/assembler/parser.c"
    break;

  case 82: /* instruction: T_STMH T_LABEL T_LABEL  */
#line 268 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STMH,  LabelArgument,    (yyvsp[-1].ssmLabel), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2247 "src/assembler/parser.c"
    break;

  case 83: /* instruction: T_STML constant constant  */
#line 270 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STML,  ValueArgument,    (yyvsp[-1].ssmVal), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2253 "src/assembler/parser.c"
    break;

  case 84: /* instruction: T_STML T_LABEL constant  */
#line 271 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STML,  LabelArgument,    (yyvsp[-1].ssmLabel), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2259 "src/assembler/parser.c"
    break;

  case 85: /* instruction: T_STML constant T_LABEL  */
#line 272 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STML,  ValueArgument,    (yyvsp[-1].ssmVal), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2265 "src/assembler/parser.c"
    break;

  case 86: /* instruction: T_STML T_LABEL T_LABEL  */
#line 273 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STML,  LabelArgument,    (yyvsp[-1].ssmLabel), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2271 "src/assembler/parser.c"
    break;

  case 87: /* instruction: T_STMS constant constant  */
#line 275 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STMS,  ValueArgument,    (yyvsp[-1].ssmVal), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2277 "src/assembler/parser.c"
    break;

  case 88: /* instruction: T_STMS T_LABEL constant  */
#line 276 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STMS,  LabelArgument,    (yyvsp[-1].ssmLabel), ValueArgument,    (yyvsp[0].ssmVal));  }
#line 2283 "src/assembler/parser.c"
    break;

  case 89: /* instruction: T_STMS constant T_LABEL  */
#line 277 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STMS,  ValueArgument,    (yyvsp[-1].ssmVal), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2289 "src/assembler/parser.c"
    break;

  case 90: /* instruction: T_STMS T_LABEL T_LABEL  */
#line 278 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STMS,  LabelArgument,    (yyvsp[-1].ssmLabel), LabelArgument,    (yyvsp[0].ssmLabel));  }
#line 2295 "src/assembler/parser.c"
    break;

  case 91: /* instruction: T_STR register  */
#line 280 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STR,   RegisterArgument, (yyvsp[0].ssmReg));                        }
#line 2301 "src/assembler/parser.c"
    break;

  case 92: /* instruction: T_STS constant  */
#line 282 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STS,   ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 2307 "src/assembler/parser.c"
    break;

  case 93: /* instruction: T_STS T_LABEL  */
#line 283 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_STS,   LabelArgument,    (yyvsp[0].ssmLabel));                        }
#line 2313 "src/assembler/parser.c"
    break;

  case 94: /* instruction: T_SUB  */
#line 285 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_SUB);                                                }
#line 2319 "src/assembler/parser.c"
    break;

  case 95: /* instruction: T_SWP  */
#line 286 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_SWP);                                                }
#line 2325 "src/assembler/parser.c"
    break;

  case 96: /* instruction: T_SWPRR register register  */
#line 288 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_SWPRR, RegisterArgument, (yyvsp[-1].ssmReg), RegisterArgument, (yyvsp[0].ssmReg));  }
#line 2331 "src/assembler/parser.c"
    break;

  case 97: /* instruction: T_SWPR register  */
#line 289 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_SWPR,  RegisterArgument, (yyvsp[0].ssmReg));                        }
#line 2337 "src/assembler/parser.c"
    break;

  case 98: /* instruction: T_TRAP constant  */
#line 291 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_TRAP,  ValueArgument,    (yyvsp[0].ssmVal));                        }
#line 2343 "src/assembler/parser.c"
    break;

  case 99: /* instruction: T_UNLINK  */
#line 292 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_UNLINK);                                             }
#line 2349 "src/assembler/parser.c"
    break;

  case 100: /* instruction: T_XOR  */
#line 293 "src/assembler/parser.y"
                              { (yyval.node) = createInstruction(ASM_XOR);                                                }
#line 2355 "src/assembler/parser.c"
    break;

  case 101: /* register: T_R0  */
#line 297 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_R0; }
#line 2361 "src/assembler/parser.c"
    break;

  case 102: /* register: T_R1  */
#line 298 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_R1; }
#line 2367 "src/assembler/parser.c"
    break;

  case 103: /* register: T_R2  */
#line 299 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_R2; }
#line 2373 "src/assembler/parser.c"
    break;

  case 104: /* register: T_R3  */
#line 300 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_R3; }
#line 2379 "src/assembler/parser.c"
    break;

  case 105: /* register: T_R4  */
#line 301 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_R4; }
#line 2385 "src/assembler/parser.c"
    break;

  case 106: /* register: T_R5  */
#line 302 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_R5; }
#line 2391 "src/assembler/parser.c"
    break;

  case 107: /* register: T_R6  */
#line 303 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_R6; }
#line 2397 "src/assembler/parser.c"
    break;

  case 108: /* register: T_R7  */
#line 304 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_R7; }
#line 2403 "src/assembler/parser.c"
    break;

  case 109: /* register: T_PC  */
#line 305 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_PC; }
#line 2409 "src/assembler/parser.c"
    break;

  case 110: /* register: T_SP  */
#line 306 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_SP; }
#line 2415 "src/assembler/parser.c"
    break;

  case 111: /* register: T_MP  */
#line 307 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_MP; }
#line 2421 "src/assembler/parser.c"
    break;

  case 112: /* register: T_HP  */
#line 308 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_HP; }
#line 2427 "src/assembler/parser.c"
    break;

  case 113: /* register: T_RR  */
#line 309 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_RR; }
#line 2433 "src/assembler/parser.c"
    break;

  case 114: /* register: T_SR  */
#line 310 "src/assembler/parser.y"
                                    { (yyval.ssmReg) = SSM_SR; }
#line 2439 "src/assembler/parser.c"
    break;

  case 115: /* address: T_CONSTANT  */
#line 315 "src/assembler/parser.y"
               { (yyval.ssmPtr) = (ssm_memory_ptr) (yyvsp[0].ssmVal); }
#line 2445 "src/assembler/parser.c"
    break;

  case 116: /* constant: T_CONSTANT  */
#line 320 "src/assembler/parser.y"
               { (yyval.ssmVal) = (yyvsp[0].ssmVal); }
#line 2451 "src/assembler/parser.c"
    break;


#line 2455 "src/assembler/parser.c"

        default: break;
      }
    if (yychar_backup != yychar)
      YY_LAC_DISCARD ("yychar change");
  }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      {
        yypcontext_t yyctx
          = {yyssp, yyesa, &yyes, &yyes_capacity, yytoken};
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        if (yychar != YYEMPTY)
          YY_LAC_ESTABLISH;
        yysyntax_error_status = yysyntax_error (&yymsg_alloc, &yymsg, &yyctx);
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == -1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *,
                             YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (yymsg)
              {
                yysyntax_error_status
                  = yysyntax_error (&yymsg_alloc, &yymsg, &yyctx);
                yymsgp = yymsg;
              }
            else
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = YYENOMEM;
              }
          }
        yyerror (program, scanner, yymsgp);
        if (yysyntax_error_status == YYENOMEM)
          YYNOMEM;
      }
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, program, scanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp, program, scanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  /* If the stack popping above didn't lose the initial context for the
     current lookahead token, the shift below will for sure.  */
  YY_LAC_DISCARD ("error recovery");

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (program, scanner, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, program, scanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp, program, scanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  if (yyes != yyesa)
    YYSTACK_FREE (yyes);
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
  return yyresult;
}

#line 323 "src/assembler/parser.y"

