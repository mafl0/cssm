#ifndef __SSM_ASSEMBLER_H__
#define __SSM_ASSEMBLER_H__

/**
 * Assemble a program. This consists of the following steps:
 *
 * 1. Pass over the program to assign offsets (starting at 0) to each node
 *    (instruction/label)
 * 2. Pass another time over the program and find all references to a label.
 *    Resolve the labels to addresses.
 * 3. Pass another time over the program, this time we convert the AST to an
 *    executable array.
 *
 * All in all it is not a very efficient assembler. But because this is 
 * educational software that is only meant to run small programs, this is fine.
 *
 * Returns the size of the @result@ array. Assembled program will be stored in @result@
 */
size_t assemble(ProgramT * p, ssm_memory_t ** result);


#endif // __SSM_ASSEMBLER_H__
