#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"
#include "interpreter/error.h"

#include "assembler/ast.h"
#include "assembler/assembler.h"

typedef struct LabelList {
  char * label;
  ssm_memory_ptr offs;
  struct LabelList * tail;
} LabelListT;

LabelListT * _labels = NULL;

static inline LabelListT * _newLabelList(char * label, ssm_memory_ptr offs) {
  LabelListT * l = malloc(sizeof(LabelListT));
  l->label = label; 
  l->offs  = offs;
  l->tail  = NULL;
  return l;
}

// Append a label to the list of labels, given a pointer to its last element.
// Returns the new pointer to the tail.
static inline LabelListT * _appendLabelList(char * label, ssm_memory_ptr offs, LabelListT * lsTailPtr) {
  char * labelCpy = strndup(label, LABEL_BUF_SZ);
  LabelListT * l = _newLabelList(labelCpy, offs);
  lsTailPtr->tail = l;
  return l;
}

/**
 * For a program p, iterate over every node in the list and assign offsets:
 * If the node is...
 *
 * an instruction: assign the offset and increment the offset counter by the
 * length of the argument list following the instruction.
 *
 * a label: assign the offset 
 */
static void _assignOffsetsAndGatherLabels(ProgramT * p) {

  // Offset counter, start at 0
  ssm_memory_ptr offs = 0;

  // Label list 
  LabelListT * labels = _newLabelList("DUMMY", 0x00) // drop it later
           , * labelsTailPtr = labels;

  bool foundFirstInstructionNode = false;

  for (ProgramT * ptr = p; ptr != NULL; ptr = ptr->tail) {
    ptr->head->offset = offs;

    switch(ptr->head->type) {
      case InstructionNode:
        if (!foundFirstInstructionNode) foundFirstInstructionNode = true;
        offs += 1 + lengthArgumentList(ptr->head->args);
        break;
      case LabelNode:
        labelsTailPtr = _appendLabelList(ptr->head->label, offs, labelsTailPtr);
        break;

      default:
        // Should not reach. Error?
        break;
    }
  }

  // Assign the global _labels variable. The first link was a dummy element.
  // Free the dummy element
  _labels = labels->tail;
  free(labels);

  // Error out when we have an empty program
  if (!foundFirstInstructionNode) ssmError(SSMERR_EMPTY_PROGRAM, "Error: Program is either empty or consists solely out of labels.\n");
}

/**
 * Simple linear search through the list. Error on not found.
 *
 * TODO: Make this more efficient by using a binary tree or smth...
 */
static ssm_memory_ptr _lookupLabel(char * label) {
  ssm_memory_ptr ret = (ssm_memory_ptr) NULL;

  // sentinel
  if (_labels != NULL) {
    for (LabelListT * ptr = _labels; ptr != NULL; ptr = ptr->tail) {
      if (!strcmp(ptr->label, label)) {
        ret = ptr->offs;
        return ret;;
      }
    }
  } else {
    ssmError(SSMERR_LABEL_UNRESOLVED, "Error: Could not resolve label: %s\n", label);
  }

  return ret;
}

// Traverse the program, look for all occurences of labels as arguments and
// replace them with the offsets of the labels
static void _resolveLabels(ProgramT * p) {

  /**
   * Traverse each node in the program. For each instruction node with
   * arguments, traverse those and look for label arguments. Every label
   * argument is substituted with an address argument.
   */

  for (ProgramT * programPtr = p; programPtr != NULL; programPtr = programPtr->tail) {

    if (  programPtr->head->type == InstructionNode 
       && lengthArgumentList(programPtr->head->args) > 0) {

      for (ArgumentListT * argPtr = programPtr->head->args; argPtr != NULL; argPtr = argPtr->tail) {
        if (argPtr->type == LabelArgument) {
                
          replaceArgument(argPtr, AddressArgument, (void *) _lookupLabel(argPtr->label));
          free(argPtr->label);
        }
      }
    }
  }
}

// Assemble a resolved program into a binary array
static size_t _toMachineCode(ProgramT * p, ssm_memory_t ** result) {

  // Count how large the array should be 
  size_t minimalProgramSize = 0;

  for (ProgramT * ptr = p; ptr != NULL; ptr = ptr->tail) {
    if (ptr->tail == NULL) {
      minimalProgramSize = ptr->head->offset + lengthArgumentList(ptr->head->args);
    }
  }

  // Arrays start at 0...
  ++minimalProgramSize;

  // Allocate the array and begin translation of the AST to binary
  *result = (ssm_memory_t *) calloc(sizeof(ssm_memory_t), minimalProgramSize);

  for (ProgramT * ptr = p; ptr != NULL; ptr = ptr->tail) {
    if (ptr->head->type == InstructionNode) {
      (*result)[ptr->head->offset] = ptr->head->opcode;
      
      size_t i = 1; // start at 1, because the instruction is at 0
      for (ArgumentListT * arg = ptr->head->args; arg != NULL; arg = arg->tail) {
        (*result)[ptr->head->offset + i] = getArgumentValue(arg);
        ++i;
      }
    }
  }

  return minimalProgramSize;
}

// Write the symbol table to a standard file
// TODO: incorporate in the binary? -> yes
// TODO: make debugger use the symbols.
static void _writeSymbolTable(void) {
  FILE * h = fopen("ssm_symbol_table.txt", "w");

  char lineBuffer[2*LABEL_BUF_SZ];

  for (LabelListT * l = _labels; l != NULL; l = l->tail) {
    snprintf(lineBuffer, 2*LABEL_BUF_SZ, "%08lX T %s\n", l->offs, l->label);
    fwrite(lineBuffer, strlen(lineBuffer), sizeof(char), h);
  }

  fclose(h);
}

size_t assemble(ProgramT * p, ssm_memory_t ** result) {
  _assignOffsetsAndGatherLabels(p);
  _resolveLabels(p);
  _writeSymbolTable();
  return _toMachineCode(p, result);
}
