#ifndef __SSM_AST_H__
#define __SSM_AST_H__

#include "assembler/ast/argument.h"
#include "assembler/ast/node.h"
#include "assembler/ast/program.h"

#endif // __SSM_AST_H__
