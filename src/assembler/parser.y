%{
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"
#include "ast.h"
#include "parser.h"
#include "lexer.h"

void yyerror(ProgramT ** p, yyscan_t scanner, const char * msg) {
  fprintf(stderr, "%s\n", msg); 
}

ProgramT * getAST(const char * prog) {
  ProgramT * p = NULL;
  yyscan_t scanner;
  YY_BUFFER_STATE state;

  // could not initialize
  if (yylex_init(&scanner)) return NULL;
  state = yy_scan_string(prog, scanner);

  yyset_debug(1, scanner); // for debugging

  // Parser error
  if (yyparse(&p, scanner)) return NULL;

  // Clean up
  yy_delete_buffer(state, scanner); 
  yylex_destroy(scanner);

  return p;
}

%}

// during debugging
%define parse.error detailed

%define parse.lac full
%define api.pure true
%lex-param   { yyscan_t scanner }
%parse-param { ProgramT ** program}
%parse-param { void * scanner }

%union {
  // Argument types
  ssm_memory_t   ssmVal;
  ssm_memory_ptr ssmPtr;
  char *         ssmLabel;
  ssm_register_t ssmReg;

  // Instructions and labels
  NodeT * node;

  // Program
  ProgramT * program;
}

%token T_COLON

// instructions
%token T_ADD
%token T_AJS
%token T_AND
%token T_ANNOTE
%token T_BRA
%token T_BRF
%token T_BRT
%token T_BSR
%token T_DIV
%token T_EQ
%token T_GE
%token T_GT
%token T_HALT
%token T_JSR
%token T_LDA
%token T_LDAA
%token T_LDC
%token T_LDH
%token T_LDL
%token T_LDLA
%token T_LDMA
%token T_LDMH
%token T_LDML
%token T_LDMS
%token T_LDR
%token T_LDRR
%token T_LDS
%token T_LDSA
%token T_LE
%token T_LINK
%token T_LT
%token T_MOD
%token T_MUL
%token T_NE
%token T_NEG
%token T_NOP
%token T_NOT
%token T_OR
%token T_RET
%token T_STA
%token T_STH
%token T_STL
%token T_STMA
%token T_STMH
%token T_STML
%token T_STMS
%token T_STR
%token T_STS
%token T_SUB
%token T_SWP
%token T_SWPR
%token T_SWPRR
%token T_TRAP
%token T_UNLINK
%token T_XOR

// registers
%token T_PC
%token T_HP
%token T_MP
%token T_R0
%token T_R1
%token T_R2
%token T_R3
%token T_R4
%token T_R5
%token T_R6
%token T_R7
%token T_RR
%token T_SP
%token T_SR

%token <ssmVal>   T_CONSTANT 
%token <ssmLabel> T_LABEL    

%type <ssmReg>    register
%type <node>      node
%type <node>      instruction
%type <ssmVal>    constant
%type <ssmPtr>    address

%type <program>   program
%type <program>   input

%%

input: program                { *program = $1; }

program
  : node program              { $$ = consProgram($1, $2); }
  |                           { $$ = NULL; }
  ;

/* Nodes can be labels or instructions (TODO: accept "label: instruction" on one line) */
node
  : instruction               { $$ = $1; }
  | T_LABEL T_COLON           { $$ = createLabel($1); }
  ;

instruction
  : T_ADD                     { $$ = createInstruction(ASM_ADD);                                                }

  | T_AJS  constant           { $$ = createInstruction(ASM_AJS,   ValueArgument,    $2);                        }
  | T_AJS  T_LABEL            { $$ = createInstruction(ASM_AJS,   LabelArgument,    $2);                        }

  | T_AND                     { $$ = createInstruction(ASM_AND);                                                }
  // | T_ANNOTE               { $$ = createInstruction(SSM_ANNOTE);                                             } // Skip this one for now

  | T_BRA  T_LABEL            { $$ = createInstruction(ASM_BRA,   LabelArgument,    $2);                        }
  | T_BRA  address            { $$ = createInstruction(ASM_BRA,   AddressArgument,  $2);                        }

  | T_BRF  T_LABEL            { $$ = createInstruction(ASM_BRF,   LabelArgument,    $2);                        }
  | T_BRF  address            { $$ = createInstruction(ASM_BRF,   AddressArgument,  $2);                        }

  | T_BRT  T_LABEL            { $$ = createInstruction(ASM_BRT,   LabelArgument,    $2);                        }
  | T_BRT  address            { $$ = createInstruction(ASM_BRT,   AddressArgument,  $2);                        }


  | T_BSR  T_LABEL            { $$ = createInstruction(ASM_BSR,   LabelArgument,    $2);                        }
  | T_BSR  address            { $$ = createInstruction(ASM_BSR,   AddressArgument,  $2);                        }

  | T_DIV                     { $$ = createInstruction(ASM_DIV);                                                }
  | T_EQ                      { $$ = createInstruction(ASM_EQ);                                                 }
  | T_GE                      { $$ = createInstruction(ASM_GE);                                                 }
  | T_GT                      { $$ = createInstruction(ASM_GT);                                                 }
  | T_HALT                    { $$ = createInstruction(ASM_HALT);                                               }
  | T_JSR                     { $$ = createInstruction(ASM_JSR);                                                }

  | T_LDAA constant           { $$ = createInstruction(ASM_LDAA,  ValueArgument,    $2);                        }
  | T_LDAA T_LABEL            { $$ = createInstruction(ASM_LDAA,  LabelArgument,    $2);                        }

  | T_LDA  constant           { $$ = createInstruction(ASM_LDA,   ValueArgument,    $2);                        }
  | T_LDA  T_LABEL            { $$ = createInstruction(ASM_LDA,   LabelArgument,    $2);                        }

  | T_LDC  constant           { $$ = createInstruction(ASM_LDC,   ValueArgument,    $2);                        }
  | T_LDC  T_LABEL            { $$ = createInstruction(ASM_LDC,   LabelArgument,    $2);                        }

  | T_LDH  constant           { $$ = createInstruction(ASM_LDH,   ValueArgument,    $2);                        }
  | T_LDH  T_LABEL            { $$ = createInstruction(ASM_LDH,   LabelArgument,    $2);                        }

  | T_LDLA constant           { $$ = createInstruction(ASM_LDLA,  ValueArgument,    $2);                        }
  | T_LDLA T_LABEL            { $$ = createInstruction(ASM_LDLA,  LabelArgument,    $2);                        }

  | T_LDL  constant           { $$ = createInstruction(ASM_LDL,   ValueArgument,    $2);                        }
  | T_LDL  T_LABEL            { $$ = createInstruction(ASM_LDL,   LabelArgument,    $2);                        }

  | T_LDMA constant constant  { $$ = createInstruction(ASM_LDMA,  ValueArgument,    $2, ValueArgument,    $3);  }
  | T_LDMA T_LABEL  constant  { $$ = createInstruction(ASM_LDMA,  LabelArgument,    $2, ValueArgument,    $3);  }
  | T_LDMA constant T_LABEL   { $$ = createInstruction(ASM_LDMA,  ValueArgument,    $2, LabelArgument,    $3);  }
  | T_LDMA T_LABEL  T_LABEL   { $$ = createInstruction(ASM_LDMA,  LabelArgument,    $2, LabelArgument,    $3);  }

  | T_LDMH constant constant  { $$ = createInstruction(ASM_LDMH,  ValueArgument,    $2, ValueArgument,    $3);  }
  | T_LDMH T_LABEL  constant  { $$ = createInstruction(ASM_LDMH,  LabelArgument,    $2, ValueArgument,    $3);  }
  | T_LDMH constant T_LABEL   { $$ = createInstruction(ASM_LDMH,  ValueArgument,    $2, LabelArgument,    $3);  }
  | T_LDMH T_LABEL  T_LABEL   { $$ = createInstruction(ASM_LDMH,  LabelArgument,    $2, LabelArgument,    $3);  }

  | T_LDML constant constant  { $$ = createInstruction(ASM_LDML,  ValueArgument,    $2, ValueArgument,    $3);  }
  | T_LDML T_LABEL  constant  { $$ = createInstruction(ASM_LDML,  LabelArgument,    $2, ValueArgument,    $3);  }
  | T_LDML constant T_LABEL   { $$ = createInstruction(ASM_LDML,  ValueArgument,    $2, LabelArgument,    $3);  }
  | T_LDML T_LABEL  T_LABEL   { $$ = createInstruction(ASM_LDML,  LabelArgument,    $2, LabelArgument,    $3);  }

  | T_LDMS constant constant  { $$ = createInstruction(ASM_LDMS,  ValueArgument,    $2, ValueArgument,    $3);  }
  | T_LDMS T_LABEL  constant  { $$ = createInstruction(ASM_LDMS,  LabelArgument,    $2, ValueArgument,    $3);  }
  | T_LDMS constant T_LABEL   { $$ = createInstruction(ASM_LDMS,  ValueArgument,    $2, LabelArgument,    $3);  }
  | T_LDMS T_LABEL  T_LABEL   { $$ = createInstruction(ASM_LDMS,  LabelArgument,    $2, LabelArgument,    $3);  }

  | T_LDRR register register  { $$ = createInstruction(ASM_LDRR,  RegisterArgument, $2, RegisterArgument, $3);  }
  | T_LDR  register           { $$ = createInstruction(ASM_LDR,   RegisterArgument, $2);                        }

  | T_LDS  constant           { $$ = createInstruction(ASM_LDS,   ValueArgument,    $2);                        }
  | T_LDS  T_LABEL            { $$ = createInstruction(ASM_LDS,   LabelArgument,    $2);                        }

  | T_LDSA constant           { $$ = createInstruction(ASM_LDSA,  ValueArgument,    $2);                        }
  | T_LDSA T_LABEL            { $$ = createInstruction(ASM_LDSA,  LabelArgument,    $2);                        }

  | T_LE                      { $$ = createInstruction(ASM_LE);                                                 }
  | T_LINK constant           { $$ = createInstruction(ASM_LINK,  ValueArgument,    $2);                        }
  | T_LT                      { $$ = createInstruction(ASM_LT);                                                 }
  | T_MOD                     { $$ = createInstruction(ASM_MOD);                                                }
  | T_MUL                     { $$ = createInstruction(ASM_MUL);                                                }
  | T_NE                      { $$ = createInstruction(ASM_NE);                                                 }
  | T_NEG                     { $$ = createInstruction(ASM_NEG);                                                }
  | T_NOP                     { $$ = createInstruction(ASM_NOP);                                                }
  | T_NOT                     { $$ = createInstruction(ASM_NOT);                                                }
  | T_OR                      { $$ = createInstruction(ASM_OR);                                                 }
  | T_RET                     { $$ = createInstruction(ASM_RET);                                                }

  | T_STA  constant           { $$ = createInstruction(ASM_STA,   ValueArgument,    $2);                        }
  | T_STA  T_LABEL            { $$ = createInstruction(ASM_STA,   LabelArgument,    $2);                        }

  | T_STH                     { $$ = createInstruction(ASM_STH);                                                }

  | T_STL  constant           { $$ = createInstruction(ASM_STL,   ValueArgument,    $2);                        }
  | T_STL  T_LABEL            { $$ = createInstruction(ASM_STL,   LabelArgument,    $2);                        }

  | T_STMA constant constant  { $$ = createInstruction(ASM_STMA,  ValueArgument,    $2, ValueArgument,    $3);  }
  | T_STMA T_LABEL  constant  { $$ = createInstruction(ASM_STMA,  LabelArgument,    $2, ValueArgument,    $3);  }
  | T_STMA constant T_LABEL   { $$ = createInstruction(ASM_STMA,  ValueArgument,    $2, LabelArgument,    $3);  }
  | T_STMA T_LABEL  T_LABEL   { $$ = createInstruction(ASM_STMA,  LabelArgument,    $2, LabelArgument,    $3);  }

  | T_STMH constant constant  { $$ = createInstruction(ASM_STMH,  ValueArgument,    $2, ValueArgument,    $3);  }
  | T_STMH T_LABEL  constant  { $$ = createInstruction(ASM_STMH,  LabelArgument,    $2, ValueArgument,    $3);  }
  | T_STMH constant T_LABEL   { $$ = createInstruction(ASM_STMH,  ValueArgument,    $2, LabelArgument,    $3);  }
  | T_STMH T_LABEL  T_LABEL   { $$ = createInstruction(ASM_STMH,  LabelArgument,    $2, LabelArgument,    $3);  }

  | T_STML constant constant  { $$ = createInstruction(ASM_STML,  ValueArgument,    $2, ValueArgument,    $3);  }
  | T_STML T_LABEL  constant  { $$ = createInstruction(ASM_STML,  LabelArgument,    $2, ValueArgument,    $3);  }
  | T_STML constant T_LABEL   { $$ = createInstruction(ASM_STML,  ValueArgument,    $2, LabelArgument,    $3);  }
  | T_STML T_LABEL  T_LABEL   { $$ = createInstruction(ASM_STML,  LabelArgument,    $2, LabelArgument,    $3);  }

  | T_STMS constant constant  { $$ = createInstruction(ASM_STMS,  ValueArgument,    $2, ValueArgument,    $3);  }
  | T_STMS T_LABEL  constant  { $$ = createInstruction(ASM_STMS,  LabelArgument,    $2, ValueArgument,    $3);  }
  | T_STMS constant T_LABEL   { $$ = createInstruction(ASM_STMS,  ValueArgument,    $2, LabelArgument,    $3);  }
  | T_STMS T_LABEL  T_LABEL   { $$ = createInstruction(ASM_STMS,  LabelArgument,    $2, LabelArgument,    $3);  }

  | T_STR  register           { $$ = createInstruction(ASM_STR,   RegisterArgument, $2);                        }

  | T_STS  constant           { $$ = createInstruction(ASM_STS,   ValueArgument,    $2);                        }
  | T_STS  T_LABEL            { $$ = createInstruction(ASM_STS,   LabelArgument,    $2);                        }

  | T_SUB                     { $$ = createInstruction(ASM_SUB);                                                }
  | T_SWP                     { $$ = createInstruction(ASM_SWP);                                                }

  | T_SWPRR register register { $$ = createInstruction(ASM_SWPRR, RegisterArgument, $2, RegisterArgument, $3);  }
  | T_SWPR  register          { $$ = createInstruction(ASM_SWPR,  RegisterArgument, $2);                        }

  | T_TRAP  constant          { $$ = createInstruction(ASM_TRAP,  ValueArgument,    $2);                        }
  | T_UNLINK                  { $$ = createInstruction(ASM_UNLINK);                                             }
  | T_XOR                     { $$ = createInstruction(ASM_XOR);                                                }
  ;

register
	: T_R0                      { $$ = SSM_R0; }
	| T_R1                      { $$ = SSM_R1; }
	| T_R2                      { $$ = SSM_R2; }
	| T_R3                      { $$ = SSM_R3; }
	| T_R4                      { $$ = SSM_R4; }
	| T_R5                      { $$ = SSM_R5; }
	| T_R6                      { $$ = SSM_R6; }
	| T_R7                      { $$ = SSM_R7; }
	| T_PC                      { $$ = SSM_PC; }
	| T_SP                      { $$ = SSM_SP; }
	| T_MP                      { $$ = SSM_MP; }
	| T_HP                      { $$ = SSM_HP; }
	| T_RR                      { $$ = SSM_RR; }
	| T_SR                      { $$ = SSM_SR; }
  ;

// Same as constant, but we have to cast it to the right type.
address
  : T_CONSTANT { $$ = (ssm_memory_ptr) $1; }
  ;

// Prefix immediate data with $
constant
  : T_CONSTANT { $$ = $1; }
  ;

%%
