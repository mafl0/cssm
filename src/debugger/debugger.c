#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "interpreter/model.h"
#include "interpreter/error.h"
#include "debugger/debugger.h"
#include "debugger/handlers.h"

// Handlers for debugger commands
void eval(SSM_debugger_t * ssmd, SSM_dbg_command_t cmd) {
  switch(cmd) {
    case SSM_dbg_stepi:
      dbg_handle_stepi(ssmd, 1);
      break;
    case SSM_dbg_print:
      dbg_handle_print(ssmd, 0, 0);
      break;
    case SSM_dbg_disasm:
      dbg_handle_disasm(ssmd, ssmd->ssm->pc, 5);
      break;
    case SSM_dbg_run:
      dbg_handle_run(ssmd);
      break;
    case SSM_dbg_break:
      dbg_handle_break(ssmd, 0);
      break;
    case SSM_dbg_img:
      dbg_handle_img(ssmd, NULL);
      break;
    case SSM_dbg_dump:
      dbg_handle_dump(ssmd, 0, 0);
      break;
    case SSM_dbg_previous:
      eval(ssmd, ssmd->previousCommand);
      break;
    case SSM_dbg_help:
      dbg_handle_help(ssmd);
      break;
    case SSM_dbg_quit:
      puts("Goodbye!");
      exit(EXIT_SUCCESS);
      break;
    case SSM_dbg_unknown:
    default:
      puts("Unknown debugger command. type \"help\" for information on how to use SSMdbg.");
      break;
  }

  if (cmd < SSM_dbg_previous)
    ssmd->previousCommand = cmd;
}

// This parser is shit, it'll do for now...
SSM_dbg_command_t parseDebuggerCmd(SSM_debugger_t * ssmd, const char * cmd) {

  if ( !strcmp(cmd, "examine") || !strcmp(cmd, "x") )
    return SSM_dbg_dump;

  if ( !strcmp(cmd, "stepi") || !strcmp(cmd, "si") )
    return SSM_dbg_stepi;

  if ( !strcmp(cmd, "print") || !strcmp(cmd, "p") )
    return SSM_dbg_print;

  if ( !strcmp(cmd, "disassemble") || !strcmp(cmd, "disasm") || !strcmp(cmd, "dis") )
    return SSM_dbg_disasm;

  if ( !strcmp(cmd, "delete") || !strcmp(cmd, "del") || !strcmp(cmd, "d") )
    return SSM_dbg_delete;

  if ( !strcmp(cmd, "run") || !strcmp(cmd, "r") )
    return SSM_dbg_run;

  if ( !strcmp(cmd, "image") || !strcmp(cmd, "img") )
    return SSM_dbg_img;

  if ( !strcmp(cmd, "help") || !strcmp(cmd, "h") )
    return SSM_dbg_help;

  if ( !strcmp(cmd, "quit") || !strcmp(cmd, "q") )
    return SSM_dbg_quit;

  if ( !strcmp(cmd, "") )
    return SSM_dbg_previous;

  return SSM_dbg_unknown;
}
