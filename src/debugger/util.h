#ifndef __SSM_UTIL_H__
#define __SSM_UTIL_H__

// Dump a contiguous region off SSM memory
void dump(SSM_t * ssm, ssm_memory_ptr offs, size_t size);

// Convenience function that dumps the entire segment
void dumpSegment(SSM_t * ssm, segment_t segment);

// Dump a single register
void dumpRegister(SSM_t * ssm, ssm_register_t reg);

// Dump all registers
void dumpRegisters(SSM_t * ssm);

// Disassemble a single instruction, return the number of words disassembled
size_t disasm(SSM_t * ssm, ssm_memory_ptr offs);

#endif
