/**
 * Dump a contiguous region off SSM memory
 */

#define _GNU_SOURCE

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"

// Print a single memory address
static inline void _printAddr(ssm_memory_ptr addr) {
  printf("%04X: ", (unsigned int) addr);
}

// Print a single value
static inline void _printVal(ssm_memory_t val) {
  printf("%08X ", (unsigned int) val);
}

// Return the memory section that we reside in based on a reference
static segment_t _where(SSM_t * ssm, ssm_memory_ptr addr) {
  segment_t seg;
  if (0 <= addr && addr < ssm->_stackStart) {
    seg = SSM_CODE;
  } else if (ssm->_stackStart <= addr && addr < ssm->_heapStart) {
    seg = SSM_STACK;
  } else {
    seg = SSM_HEAP;
  }

  return seg;
}

void dump(SSM_t * ssm, ssm_memory_ptr offs, size_t size) {
  
  // Work out the label, so print a nice line telling the user what is being
  // dumped. Note that we only look at the offset and not at the extent of the
  // region.
  const char * label = NULL;

  switch(_where(ssm, offs)) {
    case SSM_CODE:
      label = "code";
      break;
    case SSM_STACK:
      label = "stack";
      break;
    case SSM_HEAP:
      label = "heap";
      break;
  }

  printf("Examining %s. Range: 0x%04lX .. 0x%04lX\n", label, offs, offs+size);

  size_t j = 0;
  for (size_t i = offs; i < offs + size; i+= j) {
    
    _printAddr(i);

    j = 0;
    for (; j < 8; ++j) {
      _printVal(ssm->memory[i+j]);
    }

    putchar('\n');
  }
}

void dumpSegment(SSM_t * ssm, segment_t segment) {
  ssm_memory_ptr offs;
  size_t         size;

  switch(segment) {
    case SSM_CODE:
      offs = ssm->_codeStart;
      size = ssm->_codeSize;
      break;
    case SSM_STACK:
      offs = ssm->_stackStart;
      size = ssm->_stackSize;
      break;
    case SSM_HEAP:
      offs = ssm->_heapStart;
      size = ssm->_heapSize;
      break;
  }

  dump(ssm, offs, size);
}

void dumpRegister(SSM_t * ssm, ssm_register_t reg) {
  ssm_memory_t r;
  const char * label;

  switch(reg) {
    case SSM_PC:
      label = "PC";
      r     = ssm->pc;
      break;
    case SSM_SP:
      label = "SP";
      r     = ssm->sp;
      break;
    case SSM_MP:
      label = "MP";
      r     = ssm->mp;
      break;
    case SSM_HP:
      label = "HP";
      r     = ssm->hp;
      break;
    case SSM_RR:
      label = "RR";
      r     = ssm->rr;
      break;
    case SSM_R5:
      label = "R5";
      r     = ssm->r5;
      break;
    case SSM_R6:
      label = "R6";
      r     = ssm->r6;
      break;
    case SSM_R7:
      label = "R7";
      r     = ssm->r7;
      break;
    case SSM_SR:
      label = "SR";
      r     = ssm->sr;
      break;
  }

  printf("%s: %08X\n", label, r);
}

void dumpRegisters(SSM_t * ssm) {
  for (ssm_register_t reg = SSM_PC; reg <= SSM_SR; ++reg) {
    dumpRegister(ssm, reg);
  }
}

// TODO: rewrite this piece of shit using something extensible...
#define DISASM_STR_BUF_SIZE 128
size_t disasm(SSM_t * ssm, ssm_memory_ptr offs) {

  size_t disassembledWords = 1;
  char outbuf[DISASM_STR_BUF_SIZE];
  char * ppBuf0 = NULL
     , * ppBuf1 = NULL; // Pretty print buffer.

  switch(ssm->memory[offs]) {
    case ASM_ADD: 
      sprintf(outbuf, "%04zx: add", offs);
      break;
    case ASM_AND: 
      sprintf(outbuf, "%04zx: and", offs);
      break;
    case ASM_DIV: 
      sprintf(outbuf, "%04zx: div", offs);
      break;
    case ASM_MOD: 
      sprintf(outbuf, "%04zx: mod", offs);
      break;
    case ASM_MUL: 
      sprintf(outbuf, "%04zx: mul", offs);
      break;
    case ASM_OR: 
      sprintf(outbuf, "%04zx: or", offs);
      break;
    case ASM_SUB: 
      sprintf(outbuf, "%04zx: sub", offs);
      break;
    case ASM_XOR: 
      sprintf(outbuf, "%04zx: xor", offs);
      break;
    case ASM_EQ: 
      sprintf(outbuf, "%04zx: eq", offs);
      break;
    case ASM_NE: 
      sprintf(outbuf, "%04zx: ne", offs);
      break;
    case ASM_LT: 
      sprintf(outbuf, "%04zx: lt", offs);
      break;
    case ASM_GT: 
      sprintf(outbuf, "%04zx: gt", offs);
      break;
    case ASM_LE: 
      sprintf(outbuf, "%04zx: le", offs);
      break;
    case ASM_GE: 
      sprintf(outbuf, "%04zx: ge", offs);
      break;
    case ASM_NEG: 
      sprintf(outbuf, "%04zx: neg", offs);
      break;
    case ASM_NOT: 
      sprintf(outbuf, "%04zx: not", offs);
      break;
    case ASM_AJS: 
      sprintf(outbuf, "%04zx: ajs 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_BRA: 
      sprintf(outbuf, "%04zx: bra 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_BRF: 
      sprintf(outbuf, "%04zx: brf 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_BSR: 
      sprintf(outbuf, "%04zx: bsr 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_HALT: 
      sprintf(outbuf, "%04zx: halt", offs);
      break;
    case ASM_JSR: 
      sprintf(outbuf, "%04zx: jsr", offs);
      break;
    case ASM_LDA: 
      sprintf(outbuf, "%04zx: lda 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_LDMA: 
      sprintf(outbuf, "%04zx: ldma 0x%08x 0x%08x", 
        offs, ssm->memory[offs + 1], ssm->memory[offs + 2]);
      disassembledWords += 2;
      break;
    case ASM_LDAA: 
      sprintf(outbuf, "%04zx: ldaa 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_LDC: 
      sprintf(outbuf, "%04zx: ldc 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_LDL: 
      sprintf(outbuf, "%04zx: ldl 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_LDML: 
      sprintf(outbuf, "%04zx: ldml 0x%08x 0x%08x", 
        offs, ssm->memory[offs + 1], ssm->memory[offs + 2]);
      disassembledWords += 2;
      break;
    case ASM_LDLA: 
      sprintf(outbuf, "%04zx: ldla 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_LDR: 
      ppBuf0 = prettyPrintRegister(ssm->memory[offs + 1]);
      sprintf(outbuf, "%04zx: ldr %s", offs, ppBuf0);
      free(ppBuf0);
      disassembledWords += 1;
      break;
    case ASM_LDRR: 
      ppBuf0 = prettyPrintRegister(ssm->memory[offs + 1]);
      ppBuf1 = prettyPrintRegister(ssm->memory[offs + 2]);
      sprintf(outbuf, "%04zx: ldrr %s %s", offs, ppBuf0, ppBuf1);
      free(ppBuf0);
      free(ppBuf1);
      disassembledWords += 2;
      break;
    case ASM_LDS: 
      sprintf(outbuf, "%04zx: lds 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_LDMS: 
      sprintf(outbuf, "%04zx: ldms 0x%08x 0x%08x", 
        offs, ssm->memory[offs + 1], ssm->memory[offs + 2]);
      disassembledWords += 2;
      break;
    case ASM_LDSA: 
      sprintf(outbuf, "%04zx: ldsa 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_LINK: 
      sprintf(outbuf, "%04zx: link 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_NOP: 
      sprintf(outbuf, "%04zx: nop", offs);
      break;
    case ASM_RET: 
      sprintf(outbuf, "%04zx: ret", offs);
      break;
    case ASM_STA: 
      sprintf(outbuf, "%04zx: sta 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_STMA: 
      sprintf(outbuf, "%04zx: stma 0x%08x 0x%08x", 
        offs, ssm->memory[offs + 1], ssm->memory[offs + 2]);
      disassembledWords += 2;
      break;
    case ASM_STL: 
      sprintf(outbuf, "%04zx: stl 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_STML: 
      sprintf(outbuf, "%04zx: stml 0x%08x 0x%08x", 
        offs, ssm->memory[offs + 1], ssm->memory[offs + 2]);
      disassembledWords += 2;
      break;
    case ASM_STR: 
      ppBuf0 = prettyPrintRegister(ssm->memory[offs + 1]);
      sprintf(outbuf, "%04zx: str %s", offs, ppBuf0);
      free(ppBuf0);
      disassembledWords += 1;
      break;
    case ASM_STS: 
      sprintf(outbuf, "%04zx: sts 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_STMS: 
      sprintf(outbuf, "%04zx: stms 0x%08x 0x%08x", 
        offs, ssm->memory[offs + 1], ssm->memory[offs + 2]);
      disassembledWords += 2;
      break;
    case ASM_SWP: 
      sprintf(outbuf, "%04zx: swp", offs);
      break;
    case ASM_SWPR: 
      ppBuf0 = prettyPrintRegister(ssm->memory[offs + 1]);
      sprintf(outbuf, "%04zx: swpr %s", offs, ppBuf0);
      free(ppBuf0);
      disassembledWords += 1;
      break;
    case ASM_SWPRR: 
      ppBuf0 = prettyPrintRegister(ssm->memory[offs + 1]);
      ppBuf1 = prettyPrintRegister(ssm->memory[offs + 2]);
      sprintf(outbuf, "%04zx: swprr %s %s", offs, ppBuf0, ppBuf1);
      free(ppBuf0);
      free(ppBuf1);
      disassembledWords += 2;
      break;
    case ASM_TRAP: 
      sprintf(outbuf, "%04zx: trap 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_UNLINK: 
      sprintf(outbuf, "%04zx: unlink", offs);
      break;
    case ASM_LDH: 
      sprintf(outbuf, "%04zx: ldh 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_LDMH: 
      sprintf(outbuf, "%04zx: ldmh 0x%08x 0x%08x", 
        offs, ssm->memory[offs + 1], ssm->memory[offs + 2]);
      disassembledWords += 2;
      break;
    case ASM_STH: 
      sprintf(outbuf, "%04zx: sth", offs);
      break;
    case ASM_STMH: 
      sprintf(outbuf, "%04zx: stmh 0x%08x", offs, ssm->memory[offs + 1]);
      disassembledWords += 1;
      break;
    case ASM_ANNOTE: 
      sprintf(outbuf, "%04zx: annote 0x%08x 0x%08x 0x%08x 0x%08x 0x%08x", 
        offs,
        ssm->memory[offs + 1], ssm->memory[offs + 2],
        ssm->memory[offs + 3], ssm->memory[offs + 4],
        ssm->memory[offs + 5]);
      disassembledWords += 5;
      break;
  }

  puts(outbuf);

  return disassembledWords;
}

