%{
#include <stdio.h>

#include "debugger/tokens.h"
%}

%option outfile="ssmdbgLexer.c" header-file="ssmdbgLexer.h"
%option warn nodefault
%option bison-bridge

%%

[ \t\n] ;

break       T_BREAKPOINT;
breakpoint  T_BREAKPOINT;
b           T_BREAKPOINT;

disassemble T_DISASSEMBLE;
disasm      T_DISASSEMBLE;
dis         T_DISASSEMBLE;

delete      T_DELETE;
del         T_DELETE;
d           T_DELETE;

hexdump     T_DUMP;
dump        T_DUMP;

image       T_IMAGE;
img         T_IMAGE;

print       T_PRINT_DEC;
p           T_PRINT_DEC;
print/x     T_PRINT_HEX;
p/x         T_PRINT_HEX;
print/o     T_PRINT_OCT;
p/o         T_PRINT_OCT;
print/b     T_PRINT_BIN;
p/b         T_PRINT_BIN;

quit        T_QUIT;
q           T_QUIT;

run         T_RUN;
r           T_RUN;

stepi       T_STEPI;
si          T_STEPI;

help        T_HELP;
h           T_HELP;

%%

