#ifndef __SSM_DEBUGGER_H__
#define __SSM_DEBUGGER_H__

typedef enum SSM_dbg_command {
    SSM_dbg_break   // Set breakpoint
  , SSM_dbg_disasm  // Disassemble some instructions
  , SSM_dbg_dump    // Hexdump memory region
  , SSM_dbg_print   // Print values, addresses or registers
  , SSM_dbg_stepi   // Step a single, or multiple instructions forward
  , SSM_dbg_run     // Run until something special (breakpoints) happen
  , SSM_dbg_img     // Save the SSM struct image to a file
  , SSM_dbg_quit    // Quit the debugger
  , SSM_dbg_delete  // Delete a breakpoint

  , SSM_dbg_previous // On enter, repeat the previous command
  , SSM_dbg_unknown  // Unknown, show the user that they can get help
  , SSM_dbg_help     // Print the help
} SSM_dbg_command_t;

typedef struct SSM_debugger {
  SSM_dbg_command_t previousCommand;
  SSM_t * ssm;
} SSM_debugger_t;

void eval(SSM_debugger_t * ssmd, SSM_dbg_command_t cmd);
SSM_dbg_command_t parseDebuggerCmd(SSM_debugger_t * ssmd, const char * cmd);

#endif
