#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "interpreter/model.h"
#include "debugger/debugger.h"
#include "interpreter/error.h"
#include "debugger/util.h"
#include "interpreter/asm.h"

void dbg_handle_stepi(SSM_debugger_t * ssmd, int numSteps) {
  printf("PC @ 0x%08x, SP @ 0x%08x\n", ssmd->ssm->pc, ssmd->ssm->sp);
  disasm(ssmd->ssm, ssmd->ssm->pc);

  /* If next instruction is breakpoint then notify the user
   */
  step(ssmd->ssm);
}

void dbg_handle_print(SSM_debugger_t * ssmd, char format, ssm_memory_t * what) {

}

void dbg_handle_break(SSM_debugger_t * ssmd, ssm_memory_ptr where) {
  printf("Setting breakpoint at %08zx\n", where);

  // Add breakpoint to list of breakpoints
}

void dbg_handle_disasm(SSM_debugger_t * ssmd, ssm_memory_ptr offs, size_t amount) {
  size_t _nextInstructionOffs = 0;

  for (size_t i = 0; i < amount; ++i)
    _nextInstructionOffs += disasm(ssmd->ssm, offs + _nextInstructionOffs);
}

void dbg_handle_dump(SSM_debugger_t * ssmd, ssm_memory_ptr where, size_t ext) {
  //dump(ssmd->ssm, where, ext);
  dump(ssmd->ssm, ssmd->ssm->_stackStart, 32);
}

void dbg_handle_run(SSM_debugger_t * ssmd) {
  while (ssmd->ssm->memory[ssmd->ssm->pc] != ASM_HALT) { // (ssmd->ssm->pc not in list of breakpoints);
    step(ssmd->ssm);
  }
}

void dbg_handle_img(SSM_debugger_t * ssmd, char * filepath) {
  FILE * imageFile = fopen(filepath, "w"); 
  fclose(imageFile);
}

void dbg_handle_help(SSM_debugger_t * ssmd) {
  puts("Commands:"
       "\n"
       "\nbreakpoint <where>"
       "\n  Set a breakpoint. /where/ can be either an address, or a label."
       "\n  aliases: b, break"
       "\n"
       "\ndelete <where>"
       "\n  Delete a breakpoint. /where/ can be either an address, or a label."
       "\n  aliases: del, d"
       "\n"
       "\ndisassemble [addr [#instructions]]"
       "\n  Disassembles the instruction at /addr/. If /addr/ is omitted, will disassemble"
       "\n  the instruction at the instruction pointer instead. If /#instructions/ is given,"
       "\n  disassembles /#/ instructions from /addr/."
       "\n  aliases: dis, disasm"
       "\n"
       "\nexamine <offs> <ext>"
       "\n  Perform a hexdump of a memory region starting at the offset /offs/ with an"
       "\n  extent of /ext/"
       "\n  aliases: x"
       "\n"
       "\nimage <filename>"
       "\n  Write an image of the entire SSM state to a file."
       "\n  aliases: img"
       "\n"
       "\nprint[/format] <what>"
       "\n  print a value. Valid for format are /x for hex, /b for binary and /o for octal"
       "\n  aliases: p"
       "\n"
       "\nrun"
       "\n  Run the program until we either hit a breakpoint or execution finishes."
       "\n  aliases: r"
       "\n"
       "\nstepi [n]"
       "\n  Step /n/ instructions. Will step a single instruction if left empty"
       "\n  aliases: si"
       "\n"
       "\nhelp"
       "\n  show this help"
       "\n  aliases: h"
       "\n"
       );
}

