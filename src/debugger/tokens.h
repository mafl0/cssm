#ifndef __SSM_DBG_TOKENS_H__
#define __SSM_DBG_TOKENS_H__

typedef enum ssm_dbg_tokens {
    T_BREAKPOINT
  , T_DELETE
  , T_DISASSEMBLE
  , T_DUMP
  , T_HELP
  , T_IMAGE
  , T_PRINT_BIN
  , T_PRINT_DEC
  , T_PRINT_HEX
  , T_PRINT_OCT
  , T_QUIT
  , T_RUN
  , T_STEPI
} ssm_dbg_tokens_t;

#endif
