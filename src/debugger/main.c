#define _GNU_SOURCE

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <readline/readline.h>
#include <readline/history.h>

#include "interpreter/model.h"
#include "interpreter/asm.h"
#include "interpreter/error.h"
#include "debugger/debugger.h"

static void _usage() {
  fputs("-S stacksize\n"
        "-C codesize\n"
        "-H heapsize\n"
        "-f file that contains the compiled ssm bytecode\n"
        "-h show this help\n"
      , stderr);
}

int main(int argc, char ** argv) {
 
  size_t codeSize  = 0
       , stackSize = 0
       , heapSize  = 0;
  const char * pathname = NULL;

  char option;

  while ((option = getopt(argc, argv, "S:C:H:f:h")) != -1) {
    switch(option) {
      case 'S':
        stackSize = atoi(optarg);
        break;

      case 'C':
        codeSize = atoi(optarg);
        break;

      case 'H':
        heapSize = atoi(optarg);
        break;

      case 'f':
        pathname = optarg;
        break;

      case 'h':
        _usage();
        break;

      default:
        _usage();
        ssmError(SSMERR_OPTION_UNKNOWN, "Error: Unknown option: %s", option);
        break;
    }
  }

  // Initialise the machine, load the code and run
  SSM_t * ssm = init(codeSize, stackSize, heapSize);
  load(ssm, pathname);
  SSM_debugger_t * ssmd = malloc(sizeof(SSM_debugger_t));
  ssmd->ssm = ssm;
  ssmd->previousCommand = SSM_dbg_unknown;

  const char * prompt = "(SSMdbg) ";
  char * line;
  while (1) {
    line = readline(prompt);
    eval(ssmd, parseDebuggerCmd(ssmd, line));

    if (line && *line)
      add_history(line);
  }

  return EXIT_SUCCESS;
}
