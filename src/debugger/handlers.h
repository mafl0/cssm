#ifndef __SSM_HANDLERS_H__
#define __SSM_HANDLERS_H__

// Step @num@ instructions forward. Defaults to 1 instruction
void dbg_handle_stepi(SSM_debugger_t * ssmd, int num);

// Print a memory location or a register in a specific format
void dbg_handle_print(SSM_debugger_t * ssmd, char format, ssm_memory_t * what);

// Set a breakpoint on an address or label
void dbg_handle_break(SSM_debugger_t * ssmd, ssm_memory_ptr where);

// Delete breakpoints from an address or label
void dbg_handle_delete(SSM_debugger_t * ssmd, ssm_memory_ptr where);

// Disassemble instructions from somewhere, by default at the instruction
// pointer for @num@ instructions
void dbg_handle_disasm(SSM_debugger_t * ssmd, ssm_memory_ptr offs, size_t amount);

// Dump a memory region in hex.
void dbg_handle_dump(SSM_debugger_t * ssmd, ssm_memory_t * where, size_t ext);

// Run the program until we hit a breakpoint
void dbg_handle_run(SSM_debugger_t * ssmd);

// Make memory images of the SSM
void dbg_handle_img(SSM_debugger_t * ssmd, char * filepath);

// Print the help message 
void dbg_handle_help(SSM_debugger_t * ssmd);

#endif
