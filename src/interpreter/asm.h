#ifndef __SSM_INTERPRETER_ASM_H__
#define __SSM_INTERPRETER_ASM_H__
typedef ssm_memory_t opcode_t;

/**
 * Function to convert ssm_memory_t to an opcode_t
 */
opcode_t from_ssm_memory_t(ssm_memory_t x);

/**
 * SSM Opcode mnemonics, prefixed with ASM_ to prevent keyword collisions with C.
 */
typedef enum mnemonic { 
    ASM_ADD    = 0x01
  , ASM_AJS    = 0x64
  , ASM_AND    = 0x02
  , ASM_ANNOTE = 0xff
  , ASM_BRA    = 0x68
  , ASM_BRF    = 0x6c
  , ASM_BRT    = 0x6d
  , ASM_BSR    = 0x70
  , ASM_DIV    = 0x04
  , ASM_EQ     = 0x0e
  , ASM_GE     = 0x13
  , ASM_GT     = 0x11
  , ASM_HALT   = 0x74
  , ASM_JSR    = 0x78
  , ASM_LDA    = 0x7c
  , ASM_LDAA   = 0x80
  , ASM_LDC    = 0x84
  , ASM_LDH    = 0xd0
  , ASM_LDL    = 0x88
  , ASM_LDLA   = 0x8c
  , ASM_LDMA   = 0x7e
  , ASM_LDMH   = 0xd4
  , ASM_LDML   = 0x8a
  , ASM_LDMS   = 0x9a
  , ASM_LDR    = 0x90
  , ASM_LDRR   = 0x94
  , ASM_LDS    = 0x98
  , ASM_LDSA   = 0x9c
  , ASM_LE     = 0x12
  , ASM_LINK   = 0xa0
  , ASM_LT     = 0x10
  , ASM_MOD    = 0x07
  , ASM_MUL    = 0x08
  , ASM_NE     = 0x0f
  , ASM_NEG    = 0x20
  , ASM_NOP    = 0xa4
  , ASM_NOT    = 0x21
  , ASM_OR     = 0x09
  , ASM_RET    = 0xa8
  , ASM_STA    = 0xac
  , ASM_STH    = 0xd6
  , ASM_STL    = 0xb0
  , ASM_STMA   = 0xae
  , ASM_STMH   = 0xd8
  , ASM_STML   = 0xb2
  , ASM_STMS   = 0xba
  , ASM_STR    = 0xb4
  , ASM_STS    = 0xb8
  , ASM_SUB    = 0x0c
  , ASM_SWP    = 0xbc
  , ASM_SWPR   = 0xc0
  , ASM_SWPRR  = 0xc4
  , ASM_TRAP   = 0xc8
  , ASM_UNLINK = 0xcc
  , ASM_XOR    = 0x0d
} mnemonic_t;

// Handlers for each instruction
#define _ASM_HANDLER_PROTO(MNEMONIC) void asm_##MNEMONIC(SSM_t * ssm)

_ASM_HANDLER_PROTO(add);
_ASM_HANDLER_PROTO(and);
_ASM_HANDLER_PROTO(ajs);
_ASM_HANDLER_PROTO(annote);
_ASM_HANDLER_PROTO(bra);
_ASM_HANDLER_PROTO(brf);
_ASM_HANDLER_PROTO(brt);
_ASM_HANDLER_PROTO(bsr);
_ASM_HANDLER_PROTO(div);
_ASM_HANDLER_PROTO(eq);
_ASM_HANDLER_PROTO(ge);
_ASM_HANDLER_PROTO(gt);
_ASM_HANDLER_PROTO(halt);
_ASM_HANDLER_PROTO(jsr);
_ASM_HANDLER_PROTO(lda);
_ASM_HANDLER_PROTO(ldaa);
_ASM_HANDLER_PROTO(ldc);
_ASM_HANDLER_PROTO(ldh);
_ASM_HANDLER_PROTO(ldl);
_ASM_HANDLER_PROTO(ldla);
_ASM_HANDLER_PROTO(ldma);
_ASM_HANDLER_PROTO(ldmh);
_ASM_HANDLER_PROTO(ldml);
_ASM_HANDLER_PROTO(ldms);
_ASM_HANDLER_PROTO(ldr);
_ASM_HANDLER_PROTO(ldrr);
_ASM_HANDLER_PROTO(lds);
_ASM_HANDLER_PROTO(ldsa);
_ASM_HANDLER_PROTO(le);
_ASM_HANDLER_PROTO(link);
_ASM_HANDLER_PROTO(lt);
_ASM_HANDLER_PROTO(mod);
_ASM_HANDLER_PROTO(mul);
_ASM_HANDLER_PROTO(ne);
_ASM_HANDLER_PROTO(neg);
_ASM_HANDLER_PROTO(nop);
_ASM_HANDLER_PROTO(not);
_ASM_HANDLER_PROTO(or);
_ASM_HANDLER_PROTO(ret);
_ASM_HANDLER_PROTO(sta);
_ASM_HANDLER_PROTO(sth);
_ASM_HANDLER_PROTO(stl);
_ASM_HANDLER_PROTO(stma);
_ASM_HANDLER_PROTO(stmh);
_ASM_HANDLER_PROTO(stml);
_ASM_HANDLER_PROTO(stms);
_ASM_HANDLER_PROTO(str);
_ASM_HANDLER_PROTO(sts);
_ASM_HANDLER_PROTO(sub);
_ASM_HANDLER_PROTO(swp);
_ASM_HANDLER_PROTO(swpr);
_ASM_HANDLER_PROTO(swprr);
_ASM_HANDLER_PROTO(trap);
_ASM_HANDLER_PROTO(unlink);
_ASM_HANDLER_PROTO(xor);

#endif // __SSM_INTERPRETER_ASM_H__
