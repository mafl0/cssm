#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interpreter/error.h"

// Fatal error from which we cannot recover, with a nice error message.
void ssmError(ssm_error_t err, char * format, ...) {
  va_list argv;
  va_start(argv, format);
  vfprintf(stderr, format, argv);
  va_end(argv);

  exit(err);
}
