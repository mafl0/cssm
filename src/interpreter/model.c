/**
 * Abstract model of the machine
 */

#define _GNU_SOURCE

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "model.h"
#include "asm.h"
#include "error.h"

SSM_t * init(size_t codeSize, size_t stackSize, size_t heapSize) {
  SSM_t * ssm  = malloc(sizeof(SSM_t));

  // Initialise some bookkeeping values
  ssm->_memorySize = codeSize + stackSize + heapSize;

  ssm->_codeStart  = 0;
  ssm->_codeSize   = codeSize;

  ssm->_stackStart = codeSize;
  ssm->_stackSize  = stackSize;

  ssm->_heapStart  = codeSize + stackSize;
  ssm->_heapSize   = heapSize;

  // Initialise the registers
  ssm->pc = 0;
  ssm->sp = codeSize - 1;
  ssm->mp = codeSize - 1;
  ssm->hp = codeSize + stackSize;
  ssm->rr = 0;
  ssm->r5 = 0;
  ssm->r6 = 0;
  ssm->r7 = 0;
  ssm->sr = 0;

  // Initialise the memory, memset it to 0
  int * memory = calloc(sizeof(ssm_memory_t), ssm->_memorySize);
  memset(memory, 0, ssm->_memorySize);
  ssm->memory  = memory;

  return ssm;
}

ssm_memory_t * getRegister(SSM_t * ssm, ssm_register_t reg) {
  ssm_memory_t * retval = NULL;

  switch(reg) {
    case SSM_PC:
      retval = &ssm->pc;
      break;
    case SSM_SP:
      retval = &ssm->sp;
      break;
    case SSM_MP:
      retval = &ssm->mp;
      break;
    case SSM_HP:
      retval = &ssm->hp;
      break;
    case SSM_RR:
      retval = &ssm->rr;
      break;
    case SSM_R5:
      retval = &ssm->r5;
      break;
    case SSM_R6:
      retval = &ssm->r6;
      break;
    case SSM_R7:
      retval = &ssm->r7;
      break;
    case SSM_SR:
      retval = &ssm->sr;
      break;
    default:
      ssmError(SSMERR_UNKNOWN_REGISTER, "Error: Unknown register number: %d\n", reg);
      break;
  }

  return retval;
}

// Fetch the next instruction from the
static inline opcode_t _nextInstruction(SSM_t * ssm) {
  return from_ssm_memory_t(ssm->memory[ssm->pc]);
}

// Instruction handler
void step(SSM_t * ssm) {
  opcode_t next = _nextInstruction(ssm);

  switch(next) {
    case ASM_ADD:
      asm_add(ssm);
      break;

    case ASM_AJS:
      asm_ajs(ssm);
      break;

    case ASM_AND:
      asm_and(ssm);
      break;

    case ASM_ANNOTE:
      asm_annote(ssm);
      break;

    case ASM_BRA:
      asm_bra(ssm);
      break;

    case ASM_BRF:
      asm_brf(ssm);
      break;

    case ASM_BRT:
      asm_brt(ssm);
      break;

    case ASM_BSR:
      asm_bsr(ssm);
      break;

    case ASM_DIV:
      asm_div(ssm);
      break;

    case ASM_EQ:
      asm_eq(ssm);
      break;

    case ASM_GE:
      asm_ge(ssm);
      break;

    case ASM_GT:
      asm_gt(ssm);
      break;

    case ASM_HALT:
      asm_halt(ssm);
      break;

    case ASM_JSR:
      asm_jsr(ssm);
      break;

    case ASM_LDA:
      asm_lda(ssm);
      break;

    case ASM_LDAA:
      asm_ldaa(ssm);
      break;

    case ASM_LDC:
      asm_ldc(ssm);
      break;

    case ASM_LDH:
      asm_ldh(ssm);
      break;

    case ASM_LDL:
      asm_ldl(ssm);
      break;

    case ASM_LDLA:
      asm_ldla(ssm);
      break;

    case ASM_LDMA:
      asm_ldma(ssm);
      break;

    case ASM_LDMH:
      asm_ldmh(ssm);
      break;

    case ASM_LDML:
      asm_ldml(ssm);
      break;

    case ASM_LDMS:
      asm_ldms(ssm);
      break;

    case ASM_LDR:
        asm_ldr(ssm);
      break;

    case ASM_LDRR:
      asm_ldrr(ssm);
      break;

    case ASM_LDS:
      asm_lds(ssm);
      break;

    case ASM_LDSA:
      asm_ldsa(ssm);
      break;

    case ASM_LE:
      asm_le(ssm);
      break;

    case ASM_LINK:
      asm_link(ssm);
      break;

    case ASM_LT:
      asm_lt(ssm);
      break;

    case ASM_MOD:
      asm_mod(ssm);
      break;

    case ASM_MUL:
      asm_mul(ssm);
      break;

    case ASM_NE:
      asm_ne(ssm);
      break;

    case ASM_NEG:
      asm_neg(ssm);
      break;

    case ASM_NOP:
      asm_nop(ssm);
      break;

    case ASM_NOT:
      asm_not(ssm);
      break;

    case ASM_OR:
      asm_or(ssm);
      break;

    case ASM_RET:
      asm_ret(ssm);
      break;

    case ASM_STA:
      asm_sta(ssm);
      break;

    case ASM_STH:
      asm_sth(ssm);
      break;

    case ASM_STL:
      asm_stl(ssm);
      break;

    case ASM_STMA:
      asm_stma(ssm);
      break;

    case ASM_STMH:
      asm_stmh(ssm);
      break;

    case ASM_STML:
      asm_stml(ssm);
      break;

    case ASM_STMS:
      asm_stms(ssm);
      break;

    case ASM_STR:
      asm_str(ssm);
      break;

    case ASM_STS:
      asm_sts(ssm);
      break;

    case ASM_SUB:
      asm_sub(ssm);
      break;

    case ASM_SWP:
      asm_swp(ssm);
      break;

    case ASM_SWPR:
      asm_swpr(ssm);
      break;

    case ASM_SWPRR:
      asm_swprr(ssm);
      break;

    case ASM_TRAP:
      asm_trap(ssm);
      break;

    case ASM_UNLINK:
      asm_unlink(ssm);
      break;

    case ASM_XOR:
      asm_xor(ssm);
      break;

    // Unknown instruction. We cannot proceed execution so we crash.
    default:
      ssmError(SSMERR_INSTRUCTION_UNKNOWN, "Error: Unknown opcode encountered: 0x%02zx\n", next);
      break;
  }
}

// Poor man's version of mmap. Opens a file at /pathname/ and reads from it 
void load(SSM_t * ssm, const char * pathname) {
  reset(ssm);

  FILE * h = fopen(pathname, "r");
  if (h == NULL) {
    ssmError(SSMERR_FILE_NOT_FOUND, "Error: loading SSM image failed, file not found.\n");
  }
  
  fread(ssm->memory, ssm->_memorySize, sizeof(ssm_memory_t), h);
  fclose(h);
}
  
inline void reset(SSM_t * ssm) {
  memset(ssm->memory, 0, ssm->_memorySize);
}

// Pretty printers

static inline char * _newCharBuffer() {
  return calloc(sizeof(char), LABEL_BUF_SZ);
}

char * prettyPrintValue(ssm_memory_t val) {
  char * buf = _newCharBuffer();
  snprintf(buf, LABEL_BUF_SZ, "$0x%08x", val);
  return buf;
}

char * prettyPrintAddress(ssm_memory_ptr addr) {
  char * buf = _newCharBuffer();
  snprintf(buf, LABEL_BUF_SZ, "0x%08zx", addr);
  return buf;
}

char * prettyPrintRegister(ssm_register_t _reg) {
  char * buf = _newCharBuffer();
  const char * reg = NULL;

  switch (_reg) {
    case SSM_PC:
      reg = "PC";
      break;
    case SSM_SP:
      reg = "SP";
      break;
    case SSM_MP:
      reg = "MP";
      break;
    case SSM_HP:
      reg = "HP";
      break;
    case SSM_RR:
      reg = "RR";
      break;
    case SSM_R5:
      reg = "R5";
      break;
    case SSM_R6:
      reg = "R6";
      break;
    case SSM_R7:
      reg = "R7";
      break;
    case SSM_SR:
      reg = "SR";
      break;
  }

  snprintf(buf, LABEL_BUF_SZ, "%s", reg);
  return buf;
}
