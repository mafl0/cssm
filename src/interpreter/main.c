#define _GNU_SOURCE

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "model.h"
#include "asm.h"
#include "error.h"

static void _usage() {
  fputs("-S stacksize\n"
        "-C codesize\n"
        "-H heapsize\n"
        "-f file that contains the compiled ssm bytecode\n"
        "-h show this help\n"
      , stderr);
}

int main(int argc, char ** argv) {
 
  size_t codeSize  = 0
       , stackSize = 0
       , heapSize  = 0;
  const char * pathname = NULL;

  char option;

  // Parse command line options, S, C, H and f are required
  while ((option = getopt(argc, argv, "S:C:H:f:h")) != -1) {
    switch(option) {
      case 'S':
        stackSize = atoi(optarg);
        break;

      case 'C':
        codeSize = atoi(optarg);
        break;

      case 'H':
        heapSize = atoi(optarg);
        break;

      case 'f':
        pathname = optarg;
        break;

      case 'h':
        _usage();
        break;

      default:
        _usage();
        ssmError(SSMERR_OPTION_UNKNOWN, "Error: Unknown option: %s\n", option);
        break;
    }
  }

  // Initialise the machine, load the code and run
  SSM_t * ssm = init(codeSize, stackSize, heapSize);
  load(ssm, pathname);

  while (ssm->memory[ssm->pc] != ASM_HALT) {
    step(ssm);
  }

  return EXIT_SUCCESS;
}
