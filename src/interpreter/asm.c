/**
 * Implementation of the instructions
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "model.h"
#include "asm.h"
#include "trap.h"
#include "error.h"

// Helper functions

// Swap two values
static ssm_memory_t * SWAPBuffer;
#define SWAP(xPtr, yPtr) \
  *SWAPBuffer = *xPtr;  \
  *xPtr = *yPtr;         \
  *yPtr = *SWAPBuffer;     

// Pop a value from the stack, adjust the stack pointer
#define POP ssm->memory[ssm->sp--]

// Push a value on the stack, adjust the stack pointer
#define PUSH(v) ssm->memory[++ssm->sp] = v

// Advance the program counter
#define ADVANCEPC(n) ssm->pc += n

// Get inline argument values
#define ARG(n) ssm->memory[ssm->pc + n]

// Convert an integer to an opcode. Only the last byte of the integer will be
// considered and cast to an opcode.
inline opcode_t from_ssm_memory_t(ssm_memory_t x) {
  return (opcode_t) (x & 0xff);
}

// Instruction handlers

// binary functions are the same save for the name and the operator
#define _ASM_BINARY_PROTO(NAME, OPER)                   \
_ASM_HANDLER_PROTO(NAME) {                              \
  ssm->memory[ssm->sp - 1] =                            \
    ssm->memory[ssm->sp - 1] OPER ssm->memory[ssm->sp]; \
  --ssm->sp;                                            \
  ADVANCEPC(1);                                         \
}
_ASM_BINARY_PROTO(add, +)
_ASM_BINARY_PROTO(mul, *)
_ASM_BINARY_PROTO(sub, -)
_ASM_BINARY_PROTO(div, /)
_ASM_BINARY_PROTO(mod, %)
_ASM_BINARY_PROTO(xor, ^)
_ASM_BINARY_PROTO(and, &)
_ASM_BINARY_PROTO(or , |)

// Boolean functions all have the same structure save for the operator and the
// name. Slightly different from the normal BINARY operations, unfortunately.
#define _ASM_BOOL_PROTO(NAME, OPER)                      \
_ASM_HANDLER_PROTO(NAME) {                               \
  ssm->memory[ssm->sp - 1] =                             \
    (ssm->memory[ssm->sp - 1] OPER ssm->memory[ssm->sp]) \
      ? ssm_true                                         \
      : ssm_false;                                       \
  --ssm->sp;                                             \
  ADVANCEPC(1);                                          \
}                                                        
_ASM_BOOL_PROTO(ge, >=)
_ASM_BOOL_PROTO(gt, >)
_ASM_BOOL_PROTO(lt, <)
_ASM_BOOL_PROTO(le, <=)
_ASM_BOOL_PROTO(ne, !=)
_ASM_BOOL_PROTO(eq, ==)

#define _ASM_UNARY_PROTO(NAME, OPER)                \
_ASM_HANDLER_PROTO(NAME) {                          \
  ssm->memory[ssm->sp] = OPER ssm->memory[ssm->sp]; \
  ADVANCEPC(1);                               \
}
_ASM_UNARY_PROTO(not, ~)
_ASM_UNARY_PROTO(neg, -)

// brf and brt are essentially the same function, just minor differences
#define _ASM_BR_PROTO(NAME, COND)   \
_ASM_HANDLER_PROTO(NAME) {          \
  if (ssm->memory[ssm->sp] COND)    \
    ssm->pc = ARG(1);         \
  else                              \
    ADVANCEPC(2);                   \
  ssm->sp -= 1;                     \
}
_ASM_BR_PROTO(brt, != ssm_false)
_ASM_BR_PROTO(brf, == ssm_false)

_ASM_HANDLER_PROTO(ajs) {
  ssm->sp = ssm->sp + ARG(1);
  ADVANCEPC(2);
}

// Will not implement, artifact from the orignial java debugger
_ASM_HANDLER_PROTO(annote) {
  fputs("Encountered \"annote\" instruction, ignoring", stderr);
  asm_nop(ssm);
}

_ASM_HANDLER_PROTO(nop) {
  ADVANCEPC(1);
}

_ASM_HANDLER_PROTO(bra) {
  ssm->pc = ARG(1);
}

_ASM_HANDLER_PROTO(bsr) {
  PUSH(ssm->pc + 2);
  ssm->pc = ARG(1); // set program counter
}

_ASM_HANDLER_PROTO(halt) {
  fputs("SSM reached halt, end of execution.\n", stderr);
}

_ASM_HANDLER_PROTO(jsr) {
  ssm_memory_t buf = ssm->memory[ssm->sp]; // Stack arg
  ssm->memory[ssm->sp] = ssm->pc + 1;      // Push return addr
  ssm->pc = buf;
}

_ASM_HANDLER_PROTO(lda) {
  ssm_memory_ptr addr = ssm->memory[ssm->sp] + ARG(1);
  ssm->memory[ssm->sp] = ssm->memory[addr];
  ADVANCEPC(2);
}

_ASM_HANDLER_PROTO(ldaa) {
  ssm->memory[ssm->sp] += ARG(1); 
  ADVANCEPC(2);
}

_ASM_HANDLER_PROTO(ldc) {
  PUSH(ARG(1));
  ADVANCEPC(2);
}

_ASM_HANDLER_PROTO(ldh) {
  asm_lda(ssm); // These appear to be the same
}

_ASM_HANDLER_PROTO(ldl) {
  PUSH(ssm->memory[ssm->mp + ARG(1)]);
  ADVANCEPC(2);
}

_ASM_HANDLER_PROTO(ldla) {
  PUSH(ssm->mp + ARG(1));
  ADVANCEPC(2);
}

#define _MULTILOAD_PROTO(NAME, OFFS)          \
_ASM_HANDLER_PROTO(NAME) {                    \
  ssm_memory_ptr base = OFFS + ARG(1);  \
  for (size_t i = 0; i < ARG(2); ++i) { \
    PUSH(ssm->memory[base + i]);              \
  }                                           \
  ADVANCEPC(3);                               \
}

_MULTILOAD_PROTO(ldma, POP)
_MULTILOAD_PROTO(ldmh, POP)
_MULTILOAD_PROTO(ldml, ssm->mp)
_MULTILOAD_PROTO(ldms, ssm->sp)

_ASM_HANDLER_PROTO(ldr) {
  PUSH(*getRegister(ssm, ARG(1)));
  ADVANCEPC(2);
}

_ASM_HANDLER_PROTO(ldrr) {
  *getRegister(ssm, ARG(1)) = *getRegister(ssm, ARG(2));
  ADVANCEPC(3);
}

_ASM_HANDLER_PROTO(lds) {
  PUSH(ssm->memory[ssm->sp + ARG(1)]); //TODO: check
}

_ASM_HANDLER_PROTO(ldsa) {
  // ???
}

_ASM_HANDLER_PROTO(link) {
  PUSH(ssm->mp);
  ssm->mp = ssm->sp;
  ssm->sp = ssm->mp + ARG(1);
  ADVANCEPC(2);
}

_ASM_HANDLER_PROTO(unlink) {
  ssm->sp = ssm->mp - 1;
  ssm->mp = ssm->memory[ssm->mp];
  ADVANCEPC(1);
}

_ASM_HANDLER_PROTO(ret) {
  ssm->pc = POP;
}

_ASM_HANDLER_PROTO(sta) {
  ssm_memory_ptr ptr = POP;
  ssm_memory_t   v   = POP;
  ssm->memory[ptr + ARG(1)] = v;
  ADVANCEPC(2);
}

_ASM_HANDLER_PROTO(sth) {
  ssm->memory[ssm->hp] = POP;
  PUSH(ssm->hp);
  ssm->hp++;
  ADVANCEPC(1);
}

_ASM_HANDLER_PROTO(stl) {
  ssm->memory[ssm->mp + ARG(1)] = POP;
  ADVANCEPC(2);
}

#define _MULTISTORE_PROTO(NAME, OFFS)           \
_ASM_HANDLER_PROTO(NAME) {                      \
  for (size_t i = 0; i < ARG(2); ++i) {   \
    ssm->memory[OFFS + ARG(1) + i] = POP; \
  }                                             \
  ADVANCEPC(3);                                 \
}

// Maybe cannot be written as _MULTISTORE_PROTO?
_ASM_HANDLER_PROTO(stma) {
  ssm_memory_ptr addr = POP;
  for (size_t i = 0; i < ARG(2); ++i) {
    ssm->memory[addr + ARG(1) + i] = POP;
  }
  ADVANCEPC(3);
}

_MULTISTORE_PROTO(stmh, ssm->hp)
_MULTISTORE_PROTO(stml, ssm->mp)
_MULTISTORE_PROTO(stms, ssm->sp)

_ASM_HANDLER_PROTO(str) {
  *getRegister(ssm, ARG(1)) = POP;
  ADVANCEPC(2);
}

_ASM_HANDLER_PROTO(sts) {
  // might not be able to use pop because SP is updated before dereferencing on
  // the LHS
  ssm->memory[ssm->sp + ARG(1)] = ssm->memory[ssm->sp];
  --ssm->sp;
  ADVANCEPC(2);
}

_ASM_HANDLER_PROTO(swp) {
  SWAP(&ssm->memory[ssm->sp - 1], &ssm->memory[ssm->sp - 2]);
  ADVANCEPC(1);
}

_ASM_HANDLER_PROTO(swpr) {
  SWAP(getRegister(ssm, ARG(1)), &ssm->memory[ssm->sp - 1]);

  ADVANCEPC(2);
}

_ASM_HANDLER_PROTO(swprr) {
  SWAP( getRegister(ssm, ARG(1))
      , getRegister(ssm, ARG(2))
      );

  ADVANCEPC(3);
}

_ASM_HANDLER_PROTO(trap) {
  switch(ARG(1)) {
    case SSM_TRAP_PRINT_INT:
      trap_print_int(ssm);
      break;
    case SSM_TRAP_PRINT_UNICODE:
      trap_print_unicode(ssm);
      break;
    case SSM_TRAP_GET_INT:
      trap_get_int(ssm);
      break;
    case SSM_TRAP_GET_UNICODE:
      trap_get_unicode(ssm);
      break;
    case SSM_TRAP_GET_UNICODE_STRING:
      trap_get_unicode_string(ssm);
      break;
    case SSM_TRAP_GET_PATH_R:
      trap_get_path_r(ssm);
      break;
    case SSM_TRAP_GET_PATH_W:
      trap_get_path_w(ssm);
      break;
    case SSM_TRAP_GET_CHAR_FILE:
      trap_get_char_file(ssm);
      break;
    case SSM_TRAP_PUT_CHAR_FILE:
      trap_put_char_file(ssm);
      break;
    case SSM_TRAP_CLOSE_FILE:
      trap_close_file(ssm);
      break;
  }

  ADVANCEPC(2);
}
