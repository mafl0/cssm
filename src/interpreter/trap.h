#ifndef __SSM_INTERPRETER_TRAP_H__
#define __SSM_INTERPRETER_TRAP_H__

// "Syscalls" that the SSM supports
typedef enum ssm_trap {
   SSM_TRAP_PRINT_INT          =  0
 , SSM_TRAP_PRINT_UNICODE      =  1
 , SSM_TRAP_GET_INT            = 10
 , SSM_TRAP_GET_UNICODE        = 11
 , SSM_TRAP_GET_UNICODE_STRING = 12
 , SSM_TRAP_GET_PATH_R         = 20
 , SSM_TRAP_GET_PATH_W         = 21
 , SSM_TRAP_GET_CHAR_FILE      = 22
 , SSM_TRAP_PUT_CHAR_FILE      = 23
 , SSM_TRAP_CLOSE_FILE         = 24
} ssm_trap_t;

// Handlers for the various syscalls.
#define _TRAP_HANDLER_PROTO(xxx) void trap_##xxx(SSM_t * ssm)
_TRAP_HANDLER_PROTO(print_int);
_TRAP_HANDLER_PROTO(print_unicode);
_TRAP_HANDLER_PROTO(get_int);
_TRAP_HANDLER_PROTO(get_unicode);
_TRAP_HANDLER_PROTO(get_unicode_string);
_TRAP_HANDLER_PROTO(get_path_r);
_TRAP_HANDLER_PROTO(get_path_w);
_TRAP_HANDLER_PROTO(get_char_file);
_TRAP_HANDLER_PROTO(put_char_file);
_TRAP_HANDLER_PROTO(close_file);

#endif // __SSM_INTERPRETER_TRAP_H__
