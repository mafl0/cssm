#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

#include "model.h"
#include "trap.h"

static inline void _trapMsg(int trapnr) {
  fprintf(stdout, "trap %d: ", trapnr);
}

static inline ssm_memory_t _ssmGetChar() {
  return 0 | ((ssm_memory_t) getchar());
}

_TRAP_HANDLER_PROTO(print_int) {
  _trapMsg(0);
  printf("%d\n", ssm->memory[ssm->sp--]);
}

_TRAP_HANDLER_PROTO(print_unicode) {
  _trapMsg(1);
  printf("%c", ssm->memory[ssm->sp--]);
}

// TODO: Get int should default to 0 on ESC, and repeat the request until a parse succeeds.
_TRAP_HANDLER_PROTO(get_int) {
  _trapMsg(10);
  scanf("%d", &ssm->memory[++ssm->sp]);
}

// TODO: same as get int?
_TRAP_HANDLER_PROTO(get_unicode) {
  _trapMsg(11);
  scanf("%c", (char *) &ssm->memory[++ssm->sp]);
}

_TRAP_HANDLER_PROTO(get_unicode_string) {
  _trapMsg(12);

  ssm_memory_t buf[STRING_BUF_SZ];
  size_t l = 0;

  fgetws(buf, STRING_BUF_SZ, stdin);
  l = wcsnlen(buf, STRING_BUF_SZ);

  /** 
   * read a string and put it on the stack in REVERSE order, with null
   * terminator on the bottom.
   * trap 12: ABC
   * should result in a stack:
   * 0x00
   * 0x43
   * 0x42
   * 0x41
   * ____ <- SP
   */
  fgetws((ssm_memory_t *) &buf, STRING_BUF_SZ, stdin);
  l = wcsnlen((wchar_t *) &buf, STRING_BUF_SZ);

  for (size_t i = l; 0 <= i; --i)
    ssm->memory[ssm->sp + i] = buf[l - i];

  ssm->sp += l;
  // TODO: BUGGY AS SHIT
}

// TODO: Pop file path off stack;
// strings on the stack are in `reverse' order
// We know where they end, given that we know where it starts
#define _TRAP_IO_PROTO(NAME, NR, MODE) \
_TRAP_HANDLER_PROTO(NAME) {            \
  _trapMsg(NR);                        \
  ssm->_fileHandle = fopen("", MODE);  \
}      
_TRAP_IO_PROTO(get_path_r, 20, "r")
_TRAP_IO_PROTO(get_path_w, 21, "w")

_TRAP_HANDLER_PROTO(get_char_file) {
  _trapMsg(22);
  ssm->memory[++ssm->sp] = fgetc(ssm->_fileHandle);
}

_TRAP_HANDLER_PROTO(put_char_file) {
  _trapMsg(23);
  fputc(ssm->memory[ssm->sp--], ssm->_fileHandle);
}

_TRAP_HANDLER_PROTO(close_file) {
  _trapMsg(24);
  fclose(ssm->_fileHandle);
}
