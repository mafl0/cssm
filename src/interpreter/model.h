#ifndef __SSM_INTERPRETER_MODEL_H__
#define __SSM_INTERPRETER_MODEL_H__

#define STRING_BUF_SZ 4096
#define LABEL_BUF_SZ 128

/**
 * SSM Abstract machine model
 */

// Type for the values in an SSM memory cell and the pointers to them
typedef int32_t ssm_memory_t;
typedef size_t  ssm_memory_ptr;

// SSM Booleans
typedef enum SSMBool {
    ssm_false =  0
  , ssm_true  = -1
} SSMBool_t;

// SSM Registers
typedef enum ssm_register {
    SSM_PC = 0 // program counter
  , SSM_R0 = 0
  , SSM_SP = 1 // stack pointer
  , SSM_R1 = 1
  , SSM_MP = 2 // mark pointer
  , SSM_R2 = 2
  , SSM_HP = 3 // heap pointer
  , SSM_R3 = 3
  , SSM_RR = 4 // return register
  , SSM_R4 = 4
  , SSM_R5 = 5 // general register 5
  , SSM_R6 = 6 // general register 6
  , SSM_R7 = 7 // general register 7
  , SSM_SR = 8 // status register
} ssm_register_t;

// Enum type for SSM memory segments
typedef enum segment {
    SSM_CODE
  , SSM_STACK
  , SSM_HEAP
} segment_t;

// SSM "class"
typedef struct SSM {
  // Registers, these are /not/ memory mapped...  unfortunately :(
  ssm_memory_t pc;
  ssm_memory_t sp;
  ssm_memory_t mp;
  ssm_memory_t hp;
  ssm_memory_t rr;
  ssm_memory_t r5;
  ssm_memory_t r6;
  ssm_memory_t r7;
  ssm_memory_t sr;

  // Memory
  ssm_memory_t * memory;

  // The `trap' instruction can open a file. We store its file descriptor here.
  FILE * _fileHandle;        

  // 'Private', bookkeeping values
  size_t         _memorySize; // Size of the entire memory

  ssm_memory_ptr _codeStart;  // Offset at which the code starts, normally 0
  size_t         _codeSize;   // Extent of the memory

  ssm_memory_ptr _stackStart; // Offset at which the stack starts
  size_t         _stackSize;  // Extent of the stack

  ssm_memory_ptr _heapStart;  // Offset at which the heap starts
  size_t         _heapSize;   // Extent of the heap

} SSM_t;

/**
 * Get a pointer to a register based on a register number
 */
ssm_memory_t * getRegister(SSM_t * ssm, ssm_register_t reg);

/**
 * Initialise a machine. We will allocate the adequate amount of memory so that
 * the code, stack and heap fit and initialise all registers to point to the
 * right section.
 */
SSM_t * init(size_t codeSize, size_t stackSize, size_t heapSize);

/**
 * Resets the machine. All values in memory will be set to 0.
 */
void reset(SSM_t * ssm);

/**
 * Load a memory image. image will be copied into the memory of the ssm. If the
 * size of image is larger than the size of the memory of the ssm, it will be
 * truncated. If the converse is true, the remaining memory is set to 0.
 */
void load(SSM_t * ssm, const char * pathname);

/**
 * Single step
 */
void step(SSM_t * ssm);

/**
 * Pretty printing of values, addresses and registers
 */
char * prettyPrintValue(ssm_memory_t val);
char * prettyPrintAddress(ssm_memory_ptr addr);
char * prettyPrintRegister(ssm_register_t reg);

#endif // __SSM_INTERPRETER_MODEL_H__
