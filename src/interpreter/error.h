#ifndef __SSM_ERROR_H__
#define __SSM_ERROR_H__
/**
 * SSM Error codes
 */

typedef enum ssm_error {
    SSMERR_INSTRUCTION_UNKNOWN = 1
  , SSMERR_FILE_NOT_FOUND      = 2
  , SSMERR_OPTION_UNKNOWN      = 3
  , SSMERR_DIV_ZERO            = 4
  , SSMERR_UNKNOWN_REGISTER    = 5
  , SSMERR_LABEL_UNRESOLVED    = 6
  , SSMERR_EMPTY_PROGRAM       = 7
  , SSMERR_TRAP12_FAILED       = 8
} ssm_error_t;

void ssmError(ssm_error_t err, char * format, ...);


#endif
