################################################################################
## Simple Stack Machine                                                       ##
################################################################################

# Directories
BUILDDIR     = build
TESTBUILDDIR = $(BUILDDIR)/test
SRCDIR       = src
INTDIR       = $(SRCDIR)/interpreter
DBGDIR       = $(SRCDIR)/debugger
ASMDIR       = $(SRCDIR)/assembler
ASTDIR       = $(ASMDIR)/ast

# Specification and options for the compiler, lexer and parser 
CC          ?= gcc
CC_OPTS     := $(CC_OPTS) -Og -g -I$(SRCDIR) -L$(BUILDDIR) -Wall -Wpedantic

LEX         ?= flex
LEX_OPTS    := $(LEX_OPTS) --debug

YY          ?= bison
YY_OPTS     := $(YY_OPTS) -Wcounterexamples -t


## Targets #####################################################################


default: all
all: ctags $(BUILDDIR)/libssm.so $(BUILDDIR)/ssm $(BUILDDIR)/ssmdbg $(BUILDDIR)/ssmasm

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

$(TESTBUILDDIR): | $(BUILDDIR)
	mkdir -p $(TESTBUILDDIR)

# The functions of the SSM are compiled into a dynamic library so that the
# interpreter can be written in another language so long as the dynamic library
# is included.
SO_SRC_FILES=$(INTDIR)/model.c $(INTDIR)/asm.c $(INTDIR)/trap.c $(INTDIR)/error.c

$(BUILDDIR)/libssm.so: | $(BUILDDIR)
	$(CC) $(CC_OPTS) -fPIC -shared -o $@ $(SO_SRC_FILES)

# Pure version of the interpreter.
$(BUILDDIR)/ssm: $(BUILDDIR)/libssm.so | $(BUILDDIR)
	$(CC) $(CC_OPTS) -lssm  -o $@ $(INTDIR)/main.c

# Debugger version of the interpreter. Comes with a command line interface to
# debug SSM programs
DBG_SRC_FILES=$(DBGDIR)/main.c $(DBGDIR)/debugger.c $(DBGDIR)/util.c $(DBGDIR)/handlers.c
$(BUILDDIR)/ssmdbg: $(BUILDDIR)/libssm.so | $(BUILDDIR)
	$(CC) $(CC_OPTS) -lssm -lreadline -o $@ $(DBG_SRC_FILES)

AST_SRC_FILES=$(ASTDIR)/argument.c $(ASTDIR)/node.c $(ASTDIR)/program.c
# We need a lexer and a parser for the asesmbler, also creates lexer.h and
# parser.h
$(ASMDIR)/lexer.c: $(ASMDIR)/parser.c
	$(LEX) $(LEX_OPTS) -o $(ASMDIR)/lexer.c --header-file=$(ASMDIR)/lexer.h $(ASMDIR)/lexer.l

$(ASMDIR)/parser.c:
	$(YY) $(YY_OPTS) -o $(ASMDIR)/parser.c --defines=$(ASMDIR)/parser.h $(ASMDIR)/parser.y

ASM_SRC_FILES=$(ASMDIR)/main.c $(ASMDIR)/lexer.c $(ASMDIR)/parser.c $(ASMDIR)/assembler.c $(AST_SRC_FILES)
$(BUILDDIR)/ssmasm: $(ASM_SRC_FILES) $(BUILDDIR)/libssm.so | $(BUILDDIR)
	$(CC) $(CC_OPTS) -o $@ -lssm $(ASM_SRC_FILES)

# CTAGS for finding function definitions while working
ctags:
	ctags -f tags -R $(SRCDIR)


## TESTS #######################################################################


PARSER_TEST_FILES=test/assembler/parser-test.c $(AST_SRC_FILES)
PARSER_TEST_INCLUDE_AND_BUILD=$(ASMDIR)/lexer.c $(ASMDIR)/parser.c 
$(TESTBUILDDIR)/parser-test: $(PARSER_TEST_INCLUDE_AND_BUILD) $(BUILDDIR)/libssm.so | $(TESTBUILDDIR)
	$(CC) $(CC_OPTS) -o $@ -lssm $(PARSER_TEST_INCLUDE_AND_BUILD) $(PARSER_TEST_FILES)

ASSEMBLER_TEST_FILES=test/assembler/assembler-test.c $(ASMDIR)/assembler.c $(AST_SRC_FILES)
$(TESTBUILDDIR)/assembler-test: $(BUILDDIR)/libssm.so | $(TESTBUILDDIR)
	$(CC) $(CC_OPTS) -o $@ -lssm $(ASSEMBLER_TEST_FILES)

# Small tests for the AST files, argument, node and program
$(TESTBUILDDIR)/arguments: $(BUILDDIR)/libssm.so | $(TESTBUILDDIR)
	$(CC) $(CC_OPTS) -o $@ -lssm src/assembler/ast/argument.c test/assembler/argument-list-test.c
$(TESTBUILDDIR)/nodes: $(BUILDDIR)/libssm.so | $(TESTBUILDDIR)
	$(CC) $(CC_OPTS) -o $@ -lssm src/assembler/ast/argument.c src/assembler/ast/node.c test/assembler/test-node.c
$(TESTBUILDDIR)/programs: $(BUILDDIR)/libssm.so | $(TESTBUILDDIR)
	$(CC) $(CC_OPTS) -o $@ -lssm src/assembler/ast/argument.c src/assembler/ast/node.c src/assembler/ast/program.c test/assembler/test-program.c

.PHONY: clean
# Clean up the test build directory, the build directory and the files generated
# by flex and bison.
clean:
	rm -rf $(TESTBUILDDIR)
	rm -rf $(BUILDDIR)
	rm -f $(ASMDIR)/lexer.c $(ASMDIR)/lexer.h $(ASMDIR)/parser.c $(ASMDIR)/parser.h
