function _hexVal(x) {
  r = 0
  if (0 <= x && x <= 9) {
    r = x
  } else {
    switch (x) {
      case "a":
      case "A":
        r = 10
        break;
      case "b":
      case "B":
        r = 11
        break;
      case "c":
      case "C":
        r = 12
        break;
      case "d":
      case "D":
        r = 13
        break;
      case "e":
      case "E":
        r = 14
        break;
      case "f":
      case "F":
        r = 15
        break;
    }
  }
  return r
}
function baseConv(x, b) {
  y = 0
  for (i = 1; i <= length(x); i++) {
    val = substr(x, i, 1)
    if (b == 16) {
      val = _hexVal(val)
    }

    y = (y * b) + int(val)
  }
  return y
}
function bin2dec(x) { return baseConv(x,  2) }
function oct2dec(x) { return baseConv(x,  8) }
function hex2dec(x) { return baseConv(x, 16) }

# Remove register prefixes
/%/ {
  gsub(/%/, "", $0)
  print $0
}

/\$/ {
  
  # Everything up to the $ is the `instruction'
  instr = substr($0, 1, index($0, "$") - 1)

  num = 0
  neg = 0

  cur = index($0, "$")

  # if char after $ is -, mul = -1 and shift
  if (substr($0, cur + 1, 1) == "-") {
    neg = 1
    cur++
  } 

  switch (substr($0, cur + 1, 2)) {
    case "0b": 
      match(substr($0, cur + 3), /[0-1]+/);
      num = bin2dec(substr($0, RSTART, RLENGTH))
      break
    case "0o":
      cur += 2
      pat = /[0-7]+/
      break
    case "0x":
      cur += 2
      pat = /[0-9a-fA-F]+/
      break
    default: 
      pat = /[0-9]+/
      break
  }

  num = substr($0, RSTART, RLENGTH)

  if (neg == 0) {
    print instr num
  } else {
    print instr "-" num
  }
}

# Remove square brackets for address dereferencing
/\[.*\]/ {
  match($0, /\[[^\]*]\]/)
  print substr($0, RSTART + 1, RLENGTH - 2)
  #gsub(/[\[\]]/, "", $0)
  #print $0
}

/^;.*$/ {
  print $0
}
