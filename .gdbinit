define dPrintProgram
  if $argc == 1
    set var $addr = $arg0
    while $addr
      printf "Printing link<ProgramT> at 0x%08zx\n", $addr
      print *($addr)
      print *($addr->head)

      # Print the arguments, if any
      if ($addr->head->args)
        set var $argaddr = $addr->head->args
        while $argaddr
          print *($argaddr)
          set var $argaddr = $argaddr->tail
        end
      end

      printf "\n"

      set var $addr = $addr->tail
    end
  end
end
